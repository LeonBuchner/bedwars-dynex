package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.gamemanager.GameManager;
import de.nxtlinx.bedwars.gamemanager.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerListHandler implements Listener {

    private BedWars plugin;

    public ServerListHandler(BedWars plugin){
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this,plugin);
    }

    @EventHandler
    public void onServerPing(ServerListPingEvent event){
        if (plugin.getGameManager().getGameState() == GameState.LOBBY){
            event.setMaxPlayers(GameManager.MAX_PLAYERS);
            event.setMotd("§6Lobby");
        }
        if (plugin.getGameManager().getGameState() == GameState.INGAME){
            event.setMaxPlayers(25);
            event.setMotd("§aIngame");
        }
        if (plugin.getGameManager().getGameState() == GameState.ENDING){
            event.setMaxPlayers(25);
            event.setMotd("§cEnding");
        }
    }
}
