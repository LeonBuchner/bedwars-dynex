package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.commands.BuildCMD;
import de.nxtlinx.bedwars.gamemanager.GameManager;
import de.nxtlinx.bedwars.gamemanager.GameState;
import de.nxtlinx.bedwars.teams.TeamManager;
import de.nxtlinx.bedwars.teams.Teams;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GameProtection implements Listener {

    private BedWars plugin;

    public List<String> CHANGES;
    public ArrayList<Location> blocklocation;

    public GameProtection(BedWars plugin){
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this,plugin);
        CHANGES = new LinkedList<>();
        blocklocation = new ArrayList<>();
    }

    @EventHandler
    public void onFood(FoodLevelChangeEvent event){
        if (event.getEntity() instanceof Player){
            Player player = (Player) event.getEntity();
            if (plugin.getGameManager().getSpectators().contains(player)){
                event.setCancelled(true);
                event.setFoodLevel(20);
            }
            if (plugin.getGameManager().getGameState() != GameState.INGAME){
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onArchive(PlayerAchievementAwardedEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onArmor(PlayerArmorStandManipulateEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onEntityInteract(PlayerInteractAtEntityEvent event){

        if (event.getRightClicked().getType() == EntityType.ARMOR_STAND){
            event.setCancelled(true);
        }

        if (plugin.getGameManager().getGameState() == GameState.LOBBY){
            event.setCancelled(true);
        }
        if (plugin.getGameManager().getGameState() == GameState.ENDING){
            event.setCancelled(true);
        }
    }


    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent event) {
        event.setCancelled(true);
    }


    @EventHandler
    public void onWeather(WeatherChangeEvent event){
        event.getWorld().setGameRuleValue("randomTickSpeed","0");
        event.getWorld().setWeatherDuration(0);
        event.getWorld().setTime(2000);
        event.getWorld().setSpawnFlags(false,false);
        event.getWorld().setDifficulty(Difficulty.PEACEFUL);
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlace(BlockPlaceEvent event){
        if (!BuildCMD.build.contains(event.getPlayer())){
            if (plugin.getGameManager().getGameState() == GameState.LOBBY || plugin.getGameManager().getGameState() == GameState.ENDING){
                event.setBuild(false);
                event.setCancelled(true);
                return;
            }
            if (plugin.getGameManager().getSpectators().contains(event.getPlayer())){
                event.setCancelled(true);
                event.setBuild(false);
                return;
            }
            if (event.getBlockPlaced().getType() == Material.TNT){
                event.getBlockPlaced().setType(Material.AIR);
                TNTPrimed tntPrimed = (TNTPrimed) event.getBlockPlaced().getLocation().getWorld().spawnEntity(event.getBlockPlaced().getLocation(),EntityType.PRIMED_TNT);
                tntPrimed.setFuseTicks(25);
            }
          /*  MapManager mapManager = new MapManager();
            for (Teams teams : Teams.values()){
                if (event.getBlock().equals(mapManager.getMapSpawnBlock(BedWars.MAP,teams))){
                    event.setCancelled(true);
                    return;
                }
            } */
            blocklocation.add(event.getBlock().getLocation());
        }
    }

    @EventHandler
    public void onExplode(EntityExplodeEvent event){
        event.blockList().clear();
    }

    public void replace(){

        int size = 1;
        for (World world : Bukkit.getWorlds()){
            for (Entity entity : world.getEntities()){
                if (entity.getType() == EntityType.ARMOR_STAND){
                    size++;
                    entity.remove();
                }
                if (entity instanceof org.bukkit.entity.Item || entity instanceof Animals || entity instanceof Monster){
                    size++;
                    entity.remove();
                }
            }
        }
        Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§cRemove Entitys "+size);

        int blocks = 0;
        for (Location locations : blocklocation){
            locations.getBlock().setType(Material.AIR);
            blocks++;
        }

        for (String b: CHANGES){
            String[] blockdata = b.split(":");

            int id = Integer.parseInt(blockdata[0]);
            byte data = Byte.parseByte(blockdata[1]);
            World world = Bukkit.getWorld(blockdata[2]);
            int x = Integer.parseInt(blockdata[3]);
            int y= Integer.parseInt(blockdata[4]);
            int z = Integer.parseInt(blockdata[5]);

            world.getBlockAt(x,y,z).setTypeId(id);
            world.getBlockAt(x,y,z).setData(data);
            blocks++;
        }

        Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§cEs wurde §6"+blocks+"§c Blöcke resetet.");
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event){
        if (!BuildCMD.build.contains(event.getPlayer())){
            if (plugin.getGameManager().getGameState() == GameState.LOBBY || plugin.getGameManager().getGameState() == GameState.ENDING){
                event.setCancelled(true);
                return;
            }

            /*
                if (event.getMaterial() == Material.ARMOR_STAND){
                        event.setCancelled(true);
                    }
             */

            if (plugin.getGameManager().getSpectators().contains(event.getPlayer())){
                event.setCancelled(true);
                return;
            }
            if (!blocklocation.contains(event.getBlock().getLocation())) {
                if (event.getBlock().getType() != Material.BED_BLOCK){
                    if (event.getBlock().getData() != 31){
                        if (event.getBlock().getType() != Material.YELLOW_FLOWER){
                          //  event.getPlayer().sendMessage(BedWars.PREFIX+"§cDu kannst die Map nicht kaputt machen");
                            event.setCancelled(true);
                            return;
                        }
                    }
                }
            }
            Block b = event.getBlock();

            String block = b.getTypeId() + ":" + b.getData() + ":" + b.getWorld().getName() + ":" + b.getX() + ":" + b.getY() + ":" + b.getZ();
            CHANGES.add(block);
        }
    }

    public void addBlock(Block b){
        String block = b.getTypeId() + ":" + b.getData() + ":" + b.getWorld().getName() + ":" + b.getX() + ":" + b.getY() + ":" + b.getZ();
        CHANGES.add(block);
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event){
        if (!BuildCMD.build.contains(event.getPlayer())){
            if (plugin.getGameManager().getGameState() == GameState.LOBBY || plugin.getGameManager().getGameState() == GameState.ENDING){
                event.setCancelled(true);
            }
            if (plugin.getGameManager().getSpectators().contains(event.getPlayer())){
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onPick(PlayerPickupItemEvent event){
        if (!BuildCMD.build.contains(event.getPlayer())){
            if (plugin.getGameManager().getGameState() == GameState.LOBBY || plugin.getGameManager().getGameState() == GameState.ENDING){
                event.setCancelled(true);
            }
            if (plugin.getGameManager().getSpectators().contains(event.getPlayer())){
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onGrow(BlockGrowEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onCraft(CraftItemEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();


        if (plugin.getGameManager().getGameState() == GameState.INGAME) {

            if (plugin.getGameManager().getSpectators().contains(player)) {
                for (Player all : plugin.getGameManager().getSpectators()) {
                    all.sendMessage("§8[§7SPEC§8] §7" + player.getDisplayName() + "§8» §7" + event.getMessage());
                }
                event.setCancelled(true);
            } else {
                TeamManager teamManager = plugin.getTeamManager();
                Teams teams = teamManager.getPlayerTeam(player);                 
                if (teams != null){
                    if (GameManager.MAX_PLAYERS_PER_TEAM >= 2){
                        if (!(event.getMessage().startsWith("@a") || event.getMessage().startsWith("@all"))){
                            event.setCancelled(true);
                            for (Player teamMembers : teams.getTeamPlayers()){
                                teamMembers.sendMessage("§8[§7" + teams.getPREFIX() + "§8] §6" + player.getName() + "§7 » §7" + event.getMessage());
                            }
                        } else {
                            if ( event.getMessage().startsWith("@all")){
                                event.setFormat("§8[§7" + teams.getPREFIX() + "§8] §6" + player.getName() + "§7 » §7" + event.getMessage().replace("@all",""));
                            }
                            if ( event.getMessage().startsWith("@a")){
                                event.setFormat("§8[§7" + teams.getPREFIX() + "§8] §6" + player.getName() + "§7 » §7" + event.getMessage().replace("@a",""));
                            }
                        }
                    } else {
                        event.setFormat("§8[§7" + teams.getPREFIX() + "§8] §6" + player.getName() + "§7 » §7" + event.getMessage());
                    }
                } else {
                    event.setFormat("§6" + player.getName() + "§7 » §7" + event.getMessage());
                }
            }
        } else {
            event.setFormat("§6" + player.getName() + "§7 » §7" + event.getMessage());
        }
    }

    @EventHandler
    public void onMove(final PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        if (plugin.getGameManager().getGameState() == GameState.INGAME){
            if (plugin.getGameManager().getPlayers().contains(player)) {
                for (Entity entity : player.getNearbyEntities(1,1,1)){
                    if (entity instanceof Player) {
                        Player target = (Player) entity;
                        if (player != target) {
                            if (!BuildCMD.build.contains(player))
                                if (!plugin.getGameManager().getPlayers().contains(target)) {
                                    double Ax = player.getLocation().getX();
                                    double Ay = player.getLocation().getY();
                                    double Az = player.getLocation().getZ();
                                    double Bx = target.getLocation().getX();
                                    double By = target.getLocation().getY();
                                    double Bz = target.getLocation().getZ();
                                    double x = Bx - Ax;
                                    double y = By - Ay;
                                    double z = Bz - Az;
                                    Vector v = new Vector(x, y, z).normalize().multiply(1D);
                                    target.setVelocity(v);
                                }
                        }
                    }
                }
            }
          /*  if (plugin.getGameManager().getPlayers().contains(player)){
                MapManager mapManager = new MapManager();
                if (player.getLocation().getY() <= mapManager.getDeathHigh(BedWars.MAP)){
                    player.setHealth(0);
                }
            } */
        }
    }

    public ArrayList<Location> getBlocklocation() {
        return blocklocation;
    }
}
