package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.commands.SetupCMD;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.teams.Teams;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Bed;

import java.util.HashMap;

public class SetupListener implements Listener {

    private BedWars plugin;

    private HashMap<Player,Integer> setupState;

    public SetupListener(BedWars plugin){
        this.plugin = plugin;
        this.plugin.getServer().getPluginManager().registerEvents(this,plugin);
        setupState = new HashMap<>();
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if (SetupCMD.setup.containsKey(player)){
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK){
                Block block = event.getClickedBlock();
                if (event.getClickedBlock().getType() == Material.BED_BLOCK) {
                    Bed bed = new Bed(block.getType(), block.getData());
                    if (!setupState.containsKey(player)){
                        event.setCancelled(true);
                        setupState.put(player,2);
                        block = block.getRelative(bed.getFacing());
                        Teams teams = SetupCMD.setup.get(player).getTeams();
                        String map = SetupCMD.setup.get(player).getMap();
                        new MapManager().createBed1(block.getLocation(),teams,map);
                        player.sendMessage(BedWars.PREFIX+"§7Du hast erfolgreich das Bett von §6"+teams.getPREFIX()+"§7 gesetzt.");
                        player.sendMessage(BedWars.PREFIX+"§7Interagiere bitte noch mit dem anderen Teil");
                    } else if (setupState.get(player) == 2){
                        event.setCancelled(true);
                        setupState.remove(player);
                        block = block.getRelative(bed.getFacing());
                        Teams teams = SetupCMD.setup.get(player).getTeams();
                        String map = SetupCMD.setup.get(player).getMap();
                        new MapManager().createBed2(block.getLocation(),teams,map);
                        SetupCMD.setup.remove(player);
                        player.sendMessage(BedWars.PREFIX+"§7Du hast erfolgreich das Bett von §6"+teams.getPREFIX()+"§7 gesetzt.");
                    /*    if (player.getName().equals("D151l")){
                            player.sendMessage(BedWars.PREFIX+"§7Du bist nun fertig Dominic §4!!!!!!!!!!");
                            player.sendMessage(BedWars.PREFIX+"§bIcke bin der Hacker vom Dienst");
                            player.sendMessage(BedWars.PREFIX+"§9Beeil dich mal du Arbeitschlampe");
                            player.sendMessage(BedWars.PREFIX+"§4Heil Hitler");
                            player.sendMessage(BedWars.PREFIX+"§4mimimimimi");
                            player.sendMessage(BedWars.PREFIX+"§4Heul Leise!");
                            player.sendMessage(BedWars.PREFIX+"§3Mit besten Grüßen aus dem Hause DynexMC");
                            player.sendMessage(BedWars.PREFIX+"§3Lqxa & NxtLinx");
                            player.sendMessage(BedWars.PREFIX+"§fUnd jetzt arbeite gefälligst weiter Sklave!");
                        } else */
                            player.sendMessage(BedWars.PREFIX+"§7Du bist nun fertig");
                    } else {
                        event.setCancelled(true);
                        player.sendMessage(BedWars.PREFIX+"§cDu musst mit einem Bett Interagieren.");
                    }
                  /*  if (!bed.isHeadOfBed()) {
                        block = block.getRelative(bed.getFacing());
                        Teams teams = SetupCMD.setup.get(player).getTeams();
                        String map = SetupCMD.setup.get(player).getMap();
                        new MapManager().createBed(block.getLocation(),teams,map);
                        SetupCMD.setup.remove(player);
                        player.sendMessage(BedWars.PREFIX+"§7Du hast erfolgreich das Bett von §6"+teams.getPREFIX()+"§7 gesetzt.");
                    } else {
                        event.setCancelled(true);
                        player.sendMessage(BedWars.PREFIX+"§cDu musst mit einem Bett Interagieren. §8(§7Bett Fuß§8)");
                    }*/
                }
            }
        }
    }
}

