package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.commands.BuildCMD;
import de.nxtlinx.bedwars.gamemanager.GameState;
import de.nxtlinx.bedwars.mysql.Stats;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.teams.TeamIngame;
import de.nxtlinx.bedwars.teams.TeamManager;
import de.nxtlinx.bedwars.teams.Teams;
import de.nxtlinx.bedwars.utils.ScoreUtils;
import de.nxtlinx.bedwars.utils.Utils;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.event.Listener;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerDeath implements Listener {

    private BedWars plugin;

    public PlayerDeath(BedWars plugin){
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this,plugin);
    }

    public void respawn(final Player player, int time){
        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                ((CraftPlayer)player).getHandle().playerConnection.a(new PacketPlayInClientCommand(PacketPlayInClientCommand.EnumClientCommand.PERFORM_RESPAWN));
            }
        },time);
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event){
        Player player = event.getEntity();
        Player killer = event.getEntity().getKiller();

        respawn(player,1);

        event.getDrops().clear();

        Teams playerTeam = plugin.getTeamManager().getPlayerTeam(player);

        if (plugin.getMySQL().isConnected()){
            try {
                Stats.addDeaths(player.getUniqueId().toString(),1);
                Stats.addLosses(player.getUniqueId().toString(),1);
            }catch (Exception ex){ }
        }

        if (killer == null){
            event.setDeathMessage(BedWars.PREFIX+"§7Der Spieler §7"+playerTeam.getChatColor()+player.getName()+"§7 ist gestorben.");
        } else {
            Teams killerTeam = plugin.getTeamManager().getPlayerTeam(killer);
            event.setDeathMessage(BedWars.PREFIX+"§7"+playerTeam.getChatColor()+player.getName()+"§7 wurde von §7"+killerTeam.getChatColor()+killer.getName()+"§7 getötet.");
            if (plugin.getMySQL().isConnected()){
                try {
                    Stats.addKills(killer.getUniqueId().toString(),1);
                }catch (Exception ex){ }
            }
        }

        final MapManager mapManager = new MapManager();

        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                TeamIngame teamIngameManager = plugin.getGameManager().getTeamIngameManager();
                if (teamIngameManager.getHasBed().contains(playerTeam)) {
                    try {
                        Location spawn = mapManager.getMapLocation(BedWars.MAP,playerTeam);
                        player.teleport(spawn);
                    } catch (Exception e) {
                        player.sendMessage(BedWars.PREFIX + "§cDer Spawn wurde noch nicht gesetzt.");
                    }
               //     player.sendMessage(BedWars.PREFIX+"§7Du konntest Respawnen, da dein Bett noch existiert.");

                } else {
                    teamIngameManager.getHasBed().remove(playerTeam);
                    try {
                        Location spec = mapManager.getSpec(BedWars.MAP);
                        player.teleport(spec);
                    } catch (Exception e) {
                        player.sendMessage(BedWars.PREFIX + "§cEs gab einen Fehler im Setup, bitte melde diesen schnellstmöglich dem Team");
                    }

                    plugin.getGameManager().getTeamIngameManager().addSpectator(player);
                    plugin.getGameManager().getPlayers().remove(player);

                    player.setHealth(20);
                    player.setFoodLevel(20);

                    Utils.setSpectator(player);

                    for (Player players : plugin.getGameManager().getPlayers()) {
                        players.hidePlayer(player);
                    }

                    ScoreUtils scoreUtils = plugin.getScoreUtils();
                    scoreUtils.removeFromTeam(player);
                    scoreUtils.addTeamSpectator(player);

                    TeamManager teamManager = plugin.getTeamManager();
                    teamManager.removePlayerFromTeam(player);

                    Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                        @Override
                        public void run() {
                            if (!player.getAllowFlight()) {
                                player.setAllowFlight(true);
                            }
                            if (!player.isFlying())
                                player.setFlying(true);
                        }
                    }, 3);

                    if (check()){
                        plugin.getGameManager().startEnding();
                    } else if (plugin.getGameManager().getPlayers().size() == 1) {
                        plugin.getGameManager().startEnding();
                    } else {
                        Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(BedWars.PREFIX + "§7Verbleibende Spieler: §6" + plugin.getGameManager().getPlayers().size()));
                    }
                }
            }
        },2);

  //      player.setSaturation(20);


    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent event){
     /*   Player player = event.getPlayer();
        Teams playerteam = plugin.getTeamManager().getPlayerTeam(player);
        MapManager mapManager = new MapManager();

        Location spawn = mapManager.getMapLocation(BedWars.MAP,playerteam);
        Location spec = mapManager.getSpec(BedWars.MAP);
        TeamIngame teamIngameManager = plugin.getGameManager().getTeamIngameManager();
        if (teamIngameManager.isRespawn(playerteam)) {
            try {
                player.teleport(spawn);
            } catch (Exception e) {
                player.sendMessage(BedWars.PREFIX + "§cDer Spawn wurde noch nicht gesetzt.");
            }

        } else {
            //   playerteam.removePlayer(player);
            try {
                player.teleport(spec);
            } catch (Exception e) {
                player.sendMessage(BedWars.PREFIX + "§cDer Spawn wurde noch nicht gesetzt.");
            }

            plugin.getGameManager().getTeamIngameManager().addSpectator(player);
            teamIngameManager.removeAlive(playerteam);
            if (plugin.getAllTeams().size() == 1){
                plugin.getGameManager().startEnding();
            } else {
                Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(BedWars.PREFIX+"§aVerbleibende Teams §6"+teamIngameManager.getAlive().size()));
            }

            if (plugin.getGameManager().getPlayers().size() == 1){
                plugin.getGameManager().startEnding();
            }
        }*/
    }

    @EventHandler
    public void onDamage(EntityDamageEvent event){
        if (event.getEntity() instanceof  Player){
            Player player = (Player) event.getEntity();
            if (plugin.getGameManager().getGameState() != GameState.INGAME){
                if (event.getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
                    event.setCancelled(true);
                }
                if (event.getCause().equals(EntityDamageEvent.DamageCause.VOID)) {
                    event.setCancelled(true);

                    try {
                        event.getEntity().teleport(new MapManager().getLobby());
                    } catch (Exception e) {
                        event.getEntity().sendMessage(BedWars.ERROR);
                    }

                }

                event.setCancelled(true);
                event.setDamage(0);



            } else {

                if (plugin.getGameManager().getSpectators().contains(player)) {
                    if (event.getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
                        event.setCancelled(true);
                    }
                    if (event.getCause().equals(EntityDamageEvent.DamageCause.VOID)) {
                        event.setCancelled(true);

                        try {
                            event.getEntity().teleport(new MapManager().getSpec(BedWars.MAP));
                        } catch (Exception e) {
                            event.getEntity().sendMessage(BedWars.ERROR);
                        }

                    }

                    event.setCancelled(true);
                    event.setDamage(0);
                } else {
                    if (event.getCause().equals(EntityDamageEvent.DamageCause.VOID)) {
                        event.setCancelled(true);
                        ((Player) event.getEntity()).setHealth(0);
                    }
                }
            }

        }


    }

    @EventHandler
    public void onEntityByEntity(EntityDamageByEntityEvent event){

        if (event.getEntity() instanceof Player){
            Player damager = (Player) event.getDamager();
            if (event.getDamager() instanceof Player){
                Player opfer = (Player) event.getEntity();
                if (opfer != null) {
                    if (!plugin.getGameManager().getPlayers().contains(damager) && plugin.getGameManager().getPlayers().contains(opfer)) {
                        event.setCancelled(true);
                        event.setDamage(0);
                    }
                    Teams damagerTeam = plugin.getTeamManager().getPlayerTeam(damager);
                    Teams playerTeam = plugin.getTeamManager().getPlayerTeam(opfer);
                    if (damagerTeam != null && playerTeam != null){
                        if (damagerTeam.equals(playerTeam)){
                            event.setCancelled(true);
                            event.setDamage(0);
                        }
                        if (playerTeam.equals(damagerTeam)){
                            event.setCancelled(true);
                            event.setDamage(0);
                        }
                    }
                } else {
                    event.setCancelled(true);
                    event.setDamage(0);
                }
            } else {
                event.setCancelled(true);
                event.setDamage(0);
            }
        }

        if (event.getEntity() instanceof Villager){
            Player player = (Player) event.getDamager();
            if (player.getGameMode() == GameMode.CREATIVE){
                event.setDamage(100);
            } else {
                event.setDamage(0);
                event.setCancelled(true);
            }
        }
        if (event.getEntity() instanceof ArmorStand){
            Player player = (Player) event.getDamager();
            if (BuildCMD.build.contains(player)){
                event.setDamage(100);
            } else {
                event.setDamage(0);
                event.setCancelled(true);
            }
        }
    }

    private boolean check(){
        int remove = 0;
        TeamIngame teamIngame = plugin.getGameManager().getTeamIngameManager();
        for (Teams all : plugin.getAllTeams()){
            if (!teamIngame.getHasBed().contains(all)){
                if (all.getTeamPlayers().size() == 0){
                    remove++;
                }
            }
        }
        if (remove == plugin.getAllTeams().size()-1){
            return true;
        }
        return false;
    }


    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();

        if (plugin.getGameManager().getGameState() == GameState.INGAME){
            if (plugin.getGameManager().getPlayers().contains(player)){
                double y = plugin.getGameManager().getY();
                if (player.getLocation().getY() <= y){
                    player.setHealth(0);
                }
            }
        }
    }

}
