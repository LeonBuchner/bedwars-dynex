package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.commands.BuildCMD;
import de.nxtlinx.bedwars.gamemanager.GameState;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.utils.ScoreUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;
import java.util.List;


public class PlayerInteract implements Listener {

    private BedWars plugin;

    Inventory spec = Bukkit.createInventory(null, 3 * 9, "§6Lebende Spieler");

    public PlayerInteract(BedWars plugin){
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this,plugin);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
            if (!BuildCMD.build.contains(player)){
                if (plugin.getGameManager().getGameState() == GameState.LOBBY){
                    event.setCancelled(true);
                    switch (event.getMaterial()){
                        case CLAY_BALL:
                            player.kickPlayer(BedWars.PREFIX+"§cDu hast das Spiel verlassen");
                            break;
                        case COMPASS:
                            player.openInventory(plugin.getTeamManager().getSelectionInventory());
                            break;
                        case GOLD_INGOT:
                            if (plugin.isGoldVote()){
                                if (!plugin.getVoteManager().getGoldPlayerVote().containsKey(player)){
                                    player.openInventory(plugin.getVoteManager().getVoteInventoryGold());
                                } else {
                                    player.sendMessage(BedWars.PREFIX+"§cDu hast bereits gevoted");
                                }
                            } else {
                                player.sendMessage(BedWars.PREFIX+"§7Du kannst aktuell nicht für Gold voten");
                            }
                            break;
                        case PAPER:
                            if (plugin.isMapVote()){
                                if (!plugin.getVoteManager().getPlayerVoteMap().containsKey(player)){
                                    player.openInventory(plugin.getVoteManager().getVoteInventoryMap());
                                } else {
                                    player.sendMessage(BedWars.PREFIX+"§cDu hast bereits gevoted");
                                }
                            } else {
                                player.sendMessage(BedWars.PREFIX+"§7Du kannst aktuell nicht für Maps voten");
                            }
                            break;
                        default:
                            event.setCancelled(true);
                            break;
                    }
                }
                if (plugin.getGameManager().getGameState() == GameState.INGAME){
                    if (plugin.getGameManager().getSpectators().contains(player)){
                        event.setCancelled(true);
                        switch (event.getMaterial()){
                            case CLAY_BALL:
                                player.kickPlayer(BedWars.PREFIX+"§cDu hast das Spiel verlassen");
                                break;
                            case COMPASS:
                                getTest();
                                player.openInventory(getInventory());
                                break;
                            default:
                                event.setCancelled(true);
                                break;
                        }
                        if (event.getMaterial() == Material.ARMOR_STAND){
                            event.setCancelled(true);
                        }
                    }
                }
                if (plugin.getGameManager().getGameState() == GameState.ENDING){
                    event.setCancelled(true);
                    switch (event.getMaterial()){
                        case CLAY_BALL:
                            player.kickPlayer(BedWars.PREFIX+"§cDu hast das Spiel verlassen");
                            break;
                        default:
                            event.setCancelled(true);
                            break;
                    }
                }
            }
        }
            if (event.getAction() == Action.PHYSICAL && event.getClickedBlock().getType() == Material.SOIL) {
                event.setCancelled(true);
            }
    }

    public static HashMap<String,String> teleporter = new HashMap<>();

    @EventHandler
    public void onPlayerChangeWorld(PlayerChangedWorldEvent event){
        Player player = event.getPlayer();
        if (BuildCMD.build.contains(player)){
            player.getInventory().clear();
            player.setGameMode(GameMode.CREATIVE);
        }
    }

    @EventHandler
    public void onInv(InventoryClickEvent event){
        if (event.getWhoClicked() instanceof Player){
            Player player = (Player) event.getWhoClicked();
        if (plugin.getGameManager().getGameState() == GameState.LOBBY) {
           if (!BuildCMD.build.contains(player)){
               event.setCancelled(true);
           }
            if (event.getInventory().getTitle().equals("§6ForceMap")) {
                if (event.getSlot() == event.getRawSlot()) {
                    if (event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta()) {
                        event.setCancelled(true);
                        try {
                            if (plugin.isForceMap()) {
                                MapManager mapManager = new MapManager();
                                List list = mapManager.getconfiguration().getStringList("Maps");
                                String map = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName()).toUpperCase();

                                if (list.contains(map)) {
                                    BedWars.MAP = map;
                                    player.closeInventory();
                                //    player.sendMessage(BedWars.PREFIX + "§6Du hast die Map §6" + BedWars.MAP + "§6 ausgewählt.");
                                    player.sendMessage(BedWars.PREFIX + "§7Es wird nun die Map §6"+BedWars.MAP+" §7gespielt.");
                                    plugin.setForceMap(false);
                                    plugin.setMapVote(false);
                                    for (Player all : Bukkit.getOnlinePlayers()){
                                        ScoreUtils.setPacketScore(all);
                                    }
                                } else {
                                    player.sendMessage(BedWars.PREFIX + "§cDiese Map exestiert nicht");
                                    player.closeInventory();
                                }
                            } else {
                                player.sendMessage(BedWars.PREFIX + "§cDu kannst die Map nicht mehr ändern.");
                                player.closeInventory();
                            }

                        } catch (Exception e) {
                            event.setCancelled(true);
                            player.closeInventory();
                        }
                    }
                }
            }

            if (event.getInventory().getTitle().equals("§6Map-Voting")) {
                if (event.getCurrentItem() != null){
                    if (event.getCurrentItem().getType() == Material.PAPER) {
                        event.setCancelled(true);
                        String mapName = ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName());
                        player.closeInventory();
                        plugin.getVoteManager().addVoteMap(player, mapName);
                    }
                }
            }

            if (event.getInventory().getTitle().equals("§6Gold-Voting")) {
                event.setCancelled(true);
                if (event.getCurrentItem() != null) {
                    if (event.getCurrentItem().getType() != Material.AIR) {
                        event.setCancelled(true);
                        if (event.getCurrentItem().getData().getData() == 10){
                            plugin.getVoteManager().addVoteGold(player,true);
                            player.closeInventory();
                            player.sendMessage(BedWars.PREFIX+"§7Du hast §afür §6Gold §7gevotet");
                        }
                        if (event.getCurrentItem().getData().getData() == 1){
                            plugin.getVoteManager().addVoteGold(player,false);
                            player.closeInventory();
                            player.sendMessage(BedWars.PREFIX+"§7Du hast §4gegen §6Gold §7gevotet");
                        }
                    }
                }
            }
        }
            if (plugin.getGameManager().getSpectators().contains(player)){
                if (!BuildCMD.build.contains(player)){
                    event.setCancelled(true);
                    if (event.getInventory().getTitle().equals("§6Lebende Spieler")){
                        if (event.getCurrentItem() != null){
                            if (event.getCurrentItem().getType() != Material.AIR){
                                String displayname = event.getCurrentItem().getItemMeta().getDisplayName();
                                if (teleporter.containsKey(displayname)){
                                    event.setCancelled(true);
                                    player.teleport(Bukkit.getPlayer(teleporter.get(displayname)));
                                    player.closeInventory();
                                } else {
                                    event.setCancelled(true);
                                    player.closeInventory();
                                    player.sendMessage(BedWars.ERROR);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public Inventory getInventory() {
        return spec;
    }

    public void getTest() {
        getInventory().clear();
        for (Player alive : plugin.getGameManager().getPlayers()) {
            Team team = alive.getScoreboard().getEntryTeam(alive.getName());
            ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
            //     ItemMeta meta = item.getItemMeta();
            //   meta.setDisplayName("§6§l" + alive.getName());
            SkullMeta skullMeta = (SkullMeta) item.getItemMeta();
            skullMeta.setOwner(alive.getName());
            String display = team.getPrefix()+alive.getName();
            skullMeta.setDisplayName(display);
            item.setItemMeta(skullMeta);
            getInventory().addItem(item);
            teleporter.put(display,alive.getName());

        }
    }



}
