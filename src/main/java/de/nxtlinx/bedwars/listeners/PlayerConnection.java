package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.DynexAPI;
import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.countdown.CountdownManager;
import de.nxtlinx.bedwars.countdown.LobbyCountdown;
import de.nxtlinx.bedwars.gamemanager.GameManager;
import de.nxtlinx.bedwars.gamemanager.GameState;
import de.nxtlinx.bedwars.mysql.Stats;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.teams.TeamIngame;
import de.nxtlinx.bedwars.teams.Teams;
import de.nxtlinx.bedwars.utils.ScoreUtils;
import de.nxtlinx.bedwars.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerConnection implements Listener {

    private BedWars plugin;

    public PlayerConnection(BedWars plugin){
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this,plugin);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();

        ScoreUtils.setPacketScore(player);

        GameManager gameManager = plugin.getGameManager();

        player.setLevel(plugin.getCountdownManager().getLobbyCountdown().getSeconds());

        if (gameManager.getGameState() == GameState.LOBBY){
            event.setJoinMessage(BedWars.PREFIX+"§7Der Spieler §6"+player.getName()+" §7ist §abeigetreten.");
            Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                @Override
                public void run() {
                    try {
                        player.teleport(new MapManager().getLobby());
                    } catch (Exception e) {
                        player.sendMessage(BedWars.PREFIX + "§cEs gab einen Fehler im Setup, bitte melde diesen schnellstmöglich dem Team");
                    }
                }
            }, 2);

            player.setHealth(20);
            player.setFoodLevel(20);
            Utils.setJoinItems(player);
            gameManager.getPlayers().add(player);

            ScoreUtils scoreUtils = plugin.getScoreUtils();
            scoreUtils.addPlayer(player);

            for (Player all : Bukkit.getOnlinePlayers()){
                if (!all.canSee(player)) {
                    all.showPlayer(player);
                }
            }

            CountdownManager countdownManager = plugin.getCountdownManager();

            LobbyCountdown ls = countdownManager.getLobbyCountdown();
            if(gameManager.getPlayers().size() >= GameManager.MIN_PLAYERS) {
                if(!ls.isRunning()) {
                    if(ls.isIdling())
                        ls.cancleIdle();
                    ls.run();
                    ls.setSeconds(25);
                }
            }
            if (plugin.getMySQL().isConnected()){
                try {
                    if (!Stats.playerExists(player.getUniqueId().toString())){
                        Stats.createPlayer(player.getUniqueId().toString(),player.getName());
                    }
                    Stats.setName(player.getUniqueId().toString(),player.getName());
                }catch (Exception ex){
                    plugin.getMySQL().connect();
                }
            }

        } else {
            event.setJoinMessage(null);
            gameManager.getSpectators().add(player);
            try {
                player.teleport(new MapManager().getSpec(BedWars.MAP));
            } catch (Exception e) {
                player.sendMessage(BedWars.PREFIX + "§cEs gab einen Fehler im Setup, bitte melde diesen schnellstmöglich dem Team");
            }
            Utils.setSpectator(player);
            player.setHealth(20);
            player.setFoodLevel(20);

            for (Player players : gameManager.getPlayers()){
                players.hidePlayer(player);
            }

            ScoreUtils scoreUtils = plugin.getScoreUtils();
            scoreUtils.addTeamSpectator(player);

            Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                @Override
                public void run() {
                    if (!player.getAllowFlight()){
                        player.setAllowFlight(true);
                    }
                    if (!player.isFlying())
                        player.setFlying(true);
                }
            },3);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        GameManager gameManager = plugin.getGameManager();
        if (gameManager.getGameState() == GameState.LOBBY) {
            event.setQuitMessage(BedWars.PREFIX + "§7Der Spieler §6" + player.getName() + " §7hat §cverlassen.");
            CountdownManager countdownManager = plugin.getCountdownManager();
            if (gameManager.getPlayers().contains(player)) {
                gameManager.getPlayers().remove(player);
            }
            if (countdownManager.getLobbyCountdown().isRunning()){
                if (plugin.getGameManager().getPlayers().size() < GameManager.MIN_PLAYERS){
                    countdownManager.getLobbyCountdown().cancel();
                    countdownManager.getLobbyCountdown().idle();
                }
            }
            plugin.getTeamManager().removePlayerFromTeam(player);
            plugin.getVoteManager().removeAllVotes(player);
        } else {
            event.setQuitMessage(null);
        }
        if (gameManager.getSpectators().contains(player)){
            gameManager.getSpectators().remove(player);
        }

        if (gameManager.getGameState() == GameState.INGAME){
            Teams teams = plugin.getTeamManager().getPlayerTeam(player);
            if (teams != null){
                if (teams.getTeamPlayers().size() == 1){
                    plugin.getGameManager().getTeamIngameManager().getHasBed().remove(teams);
                    for (Player all : Bukkit.getOnlinePlayers()){
                        ScoreUtils.setPacketScore(all);
                    }
                }
                plugin.getTeamManager().removePlayerFromTeam(player);
            }
                if (check()){
                    plugin.getGameManager().startEnding();
                } else if (gameManager.getPlayers().contains(player)){
                  gameManager.getPlayers().remove(player);
              if (plugin.getGameManager().getPlayers().size() == 1) {
                  plugin.getGameManager().startEnding();
              } else if (plugin.getGameManager().getTeamIngameManager().getHasBed().size() == 1) {
                  plugin.getGameManager().startEnding();
              } else {
                  Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(BedWars.PREFIX + "§7Verbleibende Spieler §6" + plugin.getGameManager().getPlayers().size()));
              }
          }
        }

    }

    @EventHandler
    public void onLogin(PlayerLoginEvent event){
        Player player = event.getPlayer();
        if ((plugin.getGameManager().getGameState() == GameState.INGAME) || (plugin.getGameManager().getGameState() == GameState.ENDING)) {
            event.allow();
        } else {
            if (plugin.getGameManager().getGameState() == GameState.LOBBY){
             /*   if (plugin.getCountdownManager().getLobbyCountdown().isStarting()){
                    event.disallow(PlayerLoginEvent.Result.KICK_OTHER,BedWars.PREFIX + "§cAktuell läuft noch der Lobby Countdown");
                } else { */
                   if (plugin.getGameManager().getPlayers().size() >= GameManager.MAX_PLAYERS) {
                         if (plugin.isDynexapi()){
                             if (!DynexAPI.getPlugin().kickPlayerNonePremi(player)){
                                     event.disallow(PlayerLoginEvent.Result.KICK_FULL, BedWars.PREFIX + "§7Du benötigst ein §chöheren§7 Rang, wenn du §7noch §7dieser §7Runde beitreten möchtest.");
                            } else {
                              event.allow();
                        }
                    } else {
                             event.disallow(PlayerLoginEvent.Result.KICK_FULL,BedWars.PREFIX+"§cDie Runde ist leider schon voll");
                         }
                   }
            //    }
            }
        }
    }

    private boolean check(){
        int remove = 0;
        TeamIngame teamIngame = plugin.getGameManager().getTeamIngameManager();
        for (Teams all : plugin.getAllTeams()){
            if (!teamIngame.getHasBed().contains(all)){
                if (all.getTeamPlayers().size() == 0){
                    remove++;
                }
            }
        }
        if (remove == plugin.getAllTeams().size()-1){
            return true;
        }
        return false;
    }
}

