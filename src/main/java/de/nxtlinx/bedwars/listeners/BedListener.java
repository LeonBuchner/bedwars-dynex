package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.commands.BuildCMD;
import de.nxtlinx.bedwars.gamemanager.GameState;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.teams.Teams;
import de.nxtlinx.bedwars.utils.ScoreUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.material.Bed;

import java.util.function.Consumer;

public class BedListener implements Listener {

    private BedWars plugin;

    public BedListener(BedWars plugin){
         this.plugin = plugin;
         this.plugin.getServer().getPluginManager().registerEvents(this,plugin);
    }

    @EventHandler
    public void onExplode(EntityExplodeEvent event){
        event.blockList().forEach(new Consumer<Block>() {
            @Override
            public void accept(Block block) {
                if (block.getType() == Material.BED_BLOCK) return;
            }
        });
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event){
        Player player = event.getPlayer();
        if (!BuildCMD.build.contains(player)) {
            if (plugin.getGameManager().getGameState() == GameState.INGAME) {
                Block block = event.getBlock();
                Teams teams = plugin.getTeamManager().getPlayerTeam(player);
                if (event.getBlock().getType() == Material.BED_BLOCK || event.getBlock().getType() == Material.BED) {
                    if (teams != null) {
                        MapManager mapManager = new MapManager();
                        Bed bed = new Bed(event.getBlock().getType(), event.getBlock().getData());
                        if (bed.isHeadOfBed()) {
                          /*    event.setCancelled(true);
                              player.sendMessage(BedWars.PREFIX + "§cAufgrund eines Fehlers kannst du nur den unteren Teil des bettes abbauen");
                              return;  */
                        } else {
                            block = block.getRelative(bed.getFacing());
                        }
                        if (mapManager.getBed1(teams, BedWars.MAP).equals(block)) {
                            event.setCancelled(true);
                            player.sendMessage(BedWars.PREFIX + "§cDu kannst nicht dein eigenes Bett abbauen.");
                            return;
                        } else if (mapManager.getBed2(teams, BedWars.MAP).equals(block)) {
                            event.setCancelled(true);
                            player.sendMessage(BedWars.PREFIX + "§cDu kannst nicht dein eigenes Bett abbauen.");
                            return;
                        } else {
                            Teams teamsBlock = MapManager.containsBedLocation(block, BedWars.MAP);
                            if (teamsBlock != null) {
                               // plugin.getGameManager().getTeamIngameManager().removeBed(teamsBlock);
                                if (teamsBlock.getTeamPlayers().size() == 0){
                                    event.setCancelled(true);
                                    return;
                                }
                                plugin.getGameManager().getTeamIngameManager().removeBed(teamsBlock);
                                    for (Player all : Bukkit.getOnlinePlayers()) {
                                        Teams allTeams = this.plugin.getTeamManager().getPlayerTeam(all);
                                        if (allTeams != null){
                                            if (allTeams.equals(teamsBlock)) {
                                                all.sendTitle("§cDein Bett", "§c wurde zerstört.");
                                                plugin.getGameManager().getTeamIngameManager().removeBed(teamsBlock);
                                            } else {
                                                all.sendMessage(BedWars.PREFIX+"§7Das Bett von "+teamsBlock.getPREFIX() + "§7 wurde von §6"+teams.getChatColor()+player.getName()+" zerstört.");
                                            }
                                        } else {
                                            all.sendMessage(BedWars.PREFIX+"§7Das Bett von "+teamsBlock.getPREFIX() + "§7 wurde von §6"+teams.getChatColor()+player.getName()+" zerstört.");
                                        }
                                        all.playSound(all.getLocation(), Sound.IRONGOLEM_DEATH, 500, Integer.MAX_VALUE);
                                    }
                                plugin.getGameManager().getTeamIngameManager().removeBed(teamsBlock);
                                    Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            for (Player allOnline : Bukkit.getOnlinePlayers()) {
                                                ScoreUtils.setPacketScore(allOnline);
                                            }
                                        }
                                    }, 7);
                               //     player.sendMessage(BedWars.PREFIX + "§aDu hast das gegenerische Bett abgebaut.");
                                    event.getBlock().setType(Material.AIR);
                                    block.setType(Material.AIR);
                                    event.getBlock().getDrops().clear();
                                    new MapManager().getBed1(teamsBlock, BedWars.MAP).setType(Material.AIR);
                                    new MapManager().getBed2(teamsBlock, BedWars.MAP).setType(Material.AIR);
                            } else {
                                event.setCancelled(true);
                                return;
                            }
                            event.setCancelled(true);
                            return;
                        }
                        //    } else
                        //        event.setCancelled(true);

                    } else
                        event.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onBed(PlayerBedEnterEvent enterEvent){
        enterEvent.setCancelled(true);
    }




    /*

    Teams allTeams = plugin.getTeamManager().getPlayerTeam(all);
                                Block gegner = mapManager.getBedLocation(allTeams,BedWars.MAP);

                                if (!gegner.equals(block)){
                                    all.sendTitle(BedWars.PREFIX+"§aDas Bett von",allTeams.getPREFIX()+"§a wurde zerstört.");
                                    all.playSound(all.getLocation(), Sound.LEVEL_UP,500,Integer.MAX_VALUE);
                                    plugin.getGameManager().getTeamIngameManager().removeBed(allTeams);
                                } else {
                                    all.sendTitle("§cDein Bett","§cwurde abgebaut.");
                                    all.playSound(all.getLocation(), Sound.AMBIENCE_THUNDER,500,Integer.MAX_VALUE);
                                    plugin.getGameManager().getTeamIngameManager().removeBed(allTeams);
                                }
     */

    @EventHandler
    public void onEnterBed(PlayerBedEnterEvent event){
        event.setCancelled(true);
    }


}