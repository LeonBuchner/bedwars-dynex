package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.gamemanager.GameState;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SpeedTeppichListener implements Listener {

    private BedWars plugin;

    public SpeedTeppichListener(BedWars plugin){
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this,plugin);
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        if (plugin.getGameManager().getGameState() == GameState.INGAME){
            if (plugin.getGameManager().getPlayers().contains(player)){
                Block blockUnder = player.getLocation().getBlock();
                if (blockUnder.getType() == Material.CARPET){
                    if (!player.hasPotionEffect(PotionEffectType.SPEED)){
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,100,5));
                    }
                }
            }
        }
    }
}
