package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.gamemanager.GameState;
import de.nxtlinx.bedwars.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class RettungsListener implements Listener {

    private BedWars plugin;

    public RettungsListener(BedWars plugin){
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this,plugin);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_AIR){
            if (event.getItem() != null){
                if (event.getItem().getType() != Material.AIR){
                    if (plugin.getGameManager().getGameState().equals(GameState.INGAME)){
                        if (event.getItem().getType() == Material.BLAZE_ROD){
                            if (plugin.getGameManager().getPlayers().contains(player)){

                                final Location playerLocation = player.getLocation();

                                final Location range = playerLocation.getDirection().normalize().multiply(1).toLocation(playerLocation.getWorld());

                                final Location block1 = new Location(playerLocation.getWorld(),playerLocation.getX()+1+2,playerLocation.getY()-6,playerLocation.getZ());
                                final Location block2 = new Location(playerLocation.getWorld(),playerLocation.getX()+2+2,playerLocation.getY()-6,playerLocation.getZ());
                                final Location block3 = new Location(playerLocation.getWorld(),playerLocation.getX()+1+2,playerLocation.getY()-6,playerLocation.getZ()+1);
                                final Location block4 = new Location(playerLocation.getWorld(),playerLocation.getX()+2+2,playerLocation.getY()-6,playerLocation.getZ()+1);
                                int size = 0;

                                if (!plugin.getGameProtection().blocklocation.contains(block1)) {
                                   size++;
                                }
                                if (!plugin.getGameProtection().blocklocation.contains(block2)) {
                                    size++;
                                }
                                if (!plugin.getGameProtection().blocklocation.contains(block3)) {
                                    size++;
                                }
                                if (!plugin.getGameProtection().blocklocation.contains(block4)) {
                                    size++;
                                }

                                if (size == 4){
                                    int place = -1;
                                    for (int i = 0; i < player.getInventory().getSize() ; i++) {
                                        if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType() == Material.BLAZE_ROD){
                                            place = i;
                                        }
                                    }
                                    if (place != -1){
                                        if (player.getInventory().getItem(place).getAmount() == 1) {
                                            player.getInventory().remove(Material.BLAZE_ROD);
                                        } else {
                                            player.getInventory().getItem(place).setAmount(player.getInventory().getItem(place).getAmount()-1);
                                        }
                                    }
                                    player.updateInventory();

                                    block1.getBlock().setType(Material.SLIME_BLOCK);
                                    block2.getBlock().setType(Material.SLIME_BLOCK);
                                    block3.getBlock().setType(Material.SLIME_BLOCK);
                                    block4.getBlock().setType(Material.SLIME_BLOCK);

                                    Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                                        @Override
                                        public void run() {
                                            block1.getBlock().setType(Material.AIR);
                                            block2.getBlock().setType(Material.AIR);
                                            block3.getBlock().setType(Material.AIR);
                                            block4.getBlock().setType(Material.AIR);
                                        }
                                    }, 50*20);

                                } else {
                                    player.sendMessage(BedWars.PREFIX+"§6"+size);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


}
