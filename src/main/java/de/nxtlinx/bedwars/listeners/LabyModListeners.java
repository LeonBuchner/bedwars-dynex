package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import net.labymod.serverapi.bukkit.event.LabyModPlayerJoinEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;

public class LabyModListeners implements Listener {

    private BedWars plugin;
    private ArrayList<Player> labyModUsers;

    public LabyModListeners(BedWars plugin){
        this.plugin = plugin;
        if (this.plugin.isLabymod()) {
            this.plugin.getServer().getPluginManager().registerEvents(this, plugin);
        }
        labyModUsers = new ArrayList<>();
    }

    @EventHandler
    public void onJoinLaby(LabyModPlayerJoinEvent event){
        Player player = event.getPlayer();
        labyModUsers.add(player);
    //    player.sendMessage(BedWars.PREFIX+"§7Du spielst mit LabyMod");

        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                plugin.getScoreUtils().addPlayer(player);
            }
        },2);
    }

    @EventHandler
    public void onLabyLeave(PlayerQuitEvent event){
        Player player = event.getPlayer();
        if (labyModUsers.contains(player)){
            labyModUsers.remove(player);
        }
    }

    public ArrayList<Player> getLabyModUsers() {
        return labyModUsers;
    }
}
