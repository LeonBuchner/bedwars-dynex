package de.nxtlinx.bedwars.listeners;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.shop.Kategorie;
import de.nxtlinx.bedwars.shop.ShopInit;
import de.nxtlinx.bedwars.shop.ShopUtils;
import de.nxtlinx.bedwars.teams.TeamManager;
import de.nxtlinx.bedwars.teams.Teams;
import de.nxtlinx.bedwars.utils.ItemBuilder;
import de.nxtlinx.bedwars.utils.ShopRemove;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.Inventory;

public class ShopListener implements Listener {

    private BedWars plugin;
    private ShopInit shopInit;

    public ShopListener(BedWars plugin){
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this,plugin);
        shopInit = plugin.getShopInit();
    }

    @EventHandler
    public void onPlayerInteractEntity(PlayerInteractAtEntityEvent event) {
       final Player player = event.getPlayer();
       if (plugin.getGameManager().getPlayers().contains(player)){
           if (event.getRightClicked().getType() == EntityType.ARMOR_STAND){
               event.setCancelled(true);
               Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
                   @Override
                   public void run() {
                       ShopInit shopInit = plugin.getShopInit();
                       player.openInventory(shopInit.openKategorie(Kategorie.SHORT));
                   }
               },1);
           }
       }
    }

    @EventHandler
    public void onInvClick(InventoryClickEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            Player player = (Player) event.getWhoClicked();
            if (event.getCurrentItem() != null){
                if (event.getCurrentItem().getType() != Material.AIR) {
                    if (event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE){
                        event.setCancelled(true);
                        return;
                    }
                    try {
                        final String displayname = event.getCurrentItem().getItemMeta().getDisplayName();
                        Inventory kategorieInv = null;
                        String shopName = null;
                        for (Kategorie kategorie : Kategorie.values()) {
                            if (event.getInventory() == kategorie.getInventory()) {
                                kategorieInv = kategorie.getInventory();
                            }
                            if (kategorie.getShopName().equals(displayname)) {
                                shopName = kategorie.getShopName();
                            }
                        }
                        ShopUtils shopUtils = null;
                        for (int i = 0; i < shopInit.getItems().size(); i++) {
                            if (shopInit.getItems().get(i).getShopName() == displayname) {
                                shopUtils = shopInit.getItems().get(i);
                            }
                        }
                        if (kategorieInv != null) {
                            event.setCancelled(true);
                        }
                        if (shopName != null) {
                            event.setCancelled(true);
                            player.openInventory(shopInit.openKategorie(shopName));
                        }
                        if (shopUtils != null) {
                            event.setCancelled(true);
                            ShopRemove shopRemove = new ShopRemove();
                            if (player.getInventory().contains(shopUtils.getPrizeType())){
                                try {
                                    if (!event.getClick().isShiftClick()){
                                        boolean remove = shopRemove.check(player, shopUtils.getPrizeType(), shopUtils.getPrize());
                                        if (remove) {
                                            TeamManager teamManager = plugin.getTeamManager();
                                            Teams teams = teamManager.getPlayerTeam(player);
                                            ChatColor chatColor = ChatColor.GRAY;
                                            if (teams != null){
                                                chatColor = teams.getChatColor();
                                            }
                                            if (shopUtils.getEnchantment() != null) {
                                                if (shopUtils.getGive() == Material.LEATHER_HELMET){
                                                    Color color = translateChatColor(player);
                                                    player.getInventory().addItem(
                                                            new ItemBuilder(shopUtils.getGive(),chatColor+"Team §7Rüstung")
                                                                    .setAmount(shopUtils.getGiveAmount())
                                                                    .addEnchantment(shopUtils.getEnchantment(), shopUtils.getLevel())
                                                                    .setLeatherColor(color)
                                                                    .buildLeatherArmour());
                                                    player.playSound(player.getLocation(), Sound.CLICK,5,Integer.MAX_VALUE);
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_CHESTPLATE){
                                                    Color color = translateChatColor(player);
                                                    player.getInventory().addItem(
                                                            new ItemBuilder(shopUtils.getGive(),chatColor+"Team §7Rüstung")
                                                                    .setAmount(shopUtils.getGiveAmount())
                                                                    .addEnchantment(shopUtils.getEnchantment(), shopUtils.getLevel())
                                                                    .setLeatherColor(color)
                                                                    .buildLeatherArmour());
                                                    player.playSound(player.getLocation(), Sound.CLICK,5,Integer.MAX_VALUE);
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_LEGGINGS){
                                                    Color color = translateChatColor(player);
                                                    player.getInventory().addItem(
                                                            new ItemBuilder(shopUtils.getGive(),chatColor+"Team §7Rüstung")
                                                                    .setAmount(shopUtils.getGiveAmount())
                                                                    .addEnchantment(shopUtils.getEnchantment(), shopUtils.getLevel())
                                                                    .setLeatherColor(color)
                                                                    .buildLeatherArmour());
                                                    player.playSound(player.getLocation(), Sound.CLICK,5,Integer.MAX_VALUE);
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_BOOTS){
                                                    Color color = translateChatColor(player);
                                                    player.getInventory().addItem(
                                                            new ItemBuilder(shopUtils.getGive(),chatColor+"Team §7Rüstung")
                                                                    .setAmount(shopUtils.getGiveAmount())
                                                                    .addEnchantment(shopUtils.getEnchantment(), shopUtils.getLevel())
                                                                    .setLeatherColor(color)
                                                                    .buildLeatherArmour());
                                                    player.playSound(player.getLocation(), Sound.CLICK,5,Integer.MAX_VALUE);
                                                    return;
                                                }
                                                player.getInventory().addItem(
                                                        new ItemBuilder(shopUtils.getGive())
                                                                .setAmount(shopUtils.getGiveAmount())
                                                                .addEnchantment(shopUtils.getEnchantment(), shopUtils.getLevel())
                                                                .build());
                                            } else {
                                                if (shopUtils.getGive() == Material.LEATHER_HELMET){
                                                    Color color = translateChatColor(player);
                                                    player.getInventory().addItem(
                                                            new ItemBuilder(shopUtils.getGive(),chatColor+"Team §7Rüstung")
                                                                    .setAmount(shopUtils.getGiveAmount())
                                                                    .setLeatherColor(color)
                                                                    .buildLeatherArmour());
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_CHESTPLATE){
                                                    Color color = translateChatColor(player);
                                                    player.getInventory().addItem(
                                                            new ItemBuilder(shopUtils.getGive(),chatColor+"Team §7Rüstung")
                                                                    .setAmount(shopUtils.getGiveAmount())
                                                                    .setLeatherColor(color)
                                                                    .buildLeatherArmour());
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_LEGGINGS){
                                                    Color color = translateChatColor(player);
                                                    player.getInventory().addItem(
                                                            new ItemBuilder(shopUtils.getGive(),chatColor+"Team §7Rüstung")
                                                                    .setAmount(shopUtils.getGiveAmount())
                                                                    .setLeatherColor(color)
                                                                    .buildLeatherArmour());
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_BOOTS){
                                                    Color color = translateChatColor(player);
                                                    player.getInventory().addItem(
                                                            new ItemBuilder(shopUtils.getGive(),chatColor+"Team §7Rüstung")
                                                                    .setAmount(shopUtils.getGiveAmount())
                                                                    .setLeatherColor(color)
                                                                    .buildLeatherArmour());
                                                    return;
                                                }
                                                player.getInventory().addItem(
                                                        new ItemBuilder(shopUtils.getGive())
                                                                .setAmount(shopUtils.getGiveAmount())
                                                                .build());
                                            }
                                            player.playSound(player.getLocation(), Sound.CLICK,5,Integer.MAX_VALUE);
                                        }
                                    } else {
                                        boolean remove = shopRemove.checkShift(player, shopUtils.getPrizeType(), shopUtils.getPrize()*5);
                                        if (remove) {
                                            if (shopUtils.getEnchantment() != null) {
                                                if (shopUtils.getGive() == Material.LEATHER_HELMET){
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_CHESTPLATE){
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_LEGGINGS){
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_BOOTS){
                                                    return;
                                                }
                                                player.getInventory().addItem(
                                                        new ItemBuilder(shopUtils.getGive())
                                                                .setAmount(shopUtils.getGiveAmount()*5)
                                                                .addEnchantment(shopUtils.getEnchantment(), shopUtils.getLevel())
                                                                .build());
                                            } else {
                                                if (shopUtils.getGive() == Material.LEATHER_HELMET){
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_CHESTPLATE){
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_LEGGINGS){
                                                    return;
                                                }
                                                if (shopUtils.getGive() == Material.LEATHER_BOOTS){
                                                    return;
                                                }
                                                player.getInventory().addItem(
                                                        new ItemBuilder(shopUtils.getGive())
                                                                .setAmount(shopUtils.getGiveAmount()*5)
                                                                .build());
                                            }
                                        }
                                        player.playSound(player.getLocation(), Sound.CLICK,5,Integer.MAX_VALUE);
                                    }
                                }catch (NullPointerException ex){
                                }
                            } else {
                                if(shopUtils.getPrizeType() == Material.IRON_INGOT){
                                    player.sendMessage(BedWars.PREFIX+"§cDu hast zu wenig §6Eisen");
                                } else if(shopUtils.getPrizeType() == Material.CLAY_BRICK){
                                    player.sendMessage(BedWars.PREFIX+"§cDu hast zu wenig §6Bronze");
                                } else if(shopUtils.getPrizeType() == Material.GOLD_INGOT){
                                    player.sendMessage(BedWars.PREFIX+"§cDu hast zu wenig §6Gold");
                                } else {
                                    player.sendMessage(BedWars.PREFIX+"§cDu hast nicht genügend Rohstoffe");
                                }
                            }

                        }
                    } catch (Exception ex) {
                    }
                }
            }
        }
    }

    private Color translateChatColor(Player player) {
        Color color = Color.GRAY;
        TeamManager teamManager = plugin.getTeamManager();
        Teams teams = teamManager.getPlayerTeam(player);
        if (teams == null){
            return color;
        }
        if (teams.getChatColor() == ChatColor.RED) {
                color = Color.RED;
        }
        if (teams.getChatColor() == ChatColor.GREEN) {
            color = Color.GREEN;
        }
        if (teams.getChatColor() == ChatColor.BLUE) {
            color = Color.BLUE;
        }
        if (teams.getChatColor() == ChatColor.YELLOW) {
            color = Color.YELLOW;
        }
        if (teams.getChatColor() == ChatColor.BLACK) {
            color = Color.BLACK;
        }
        if (teams.getChatColor() == ChatColor.DARK_PURPLE) {
            color = Color.PURPLE;
        }
        if (teams.getChatColor() == ChatColor.LIGHT_PURPLE) {
            color = Color.fromRGB(255, 192, 203);
        }
        if (teams.getChatColor() == ChatColor.GOLD) {
            color = Color.ORANGE;
        }
        return color;
    }

}
