package de.nxtlinx.bedwars.mysql;

import de.nxtlinx.bedwars.BedWars;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.Skull;

import java.util.*;

public class Top5Wall {

    private BedWars plugin;
    private Location locationPlayer1;
    private Location locationPlayer2;
    private Location locationPlayer3;
    private Location locationPlayer4;
    private Location locationPlayer5;
    private HashMap<String,Integer> allPlatz;
    private HashMap<String,Integer> top5Platz;


    public Top5Wall(BedWars plugin){
        this.plugin = plugin;
        this.allPlatz = new HashMap<>();
        this.top5Platz = new HashMap<>();
        if (plugin.getMySQL().isConnected()){
            allPlatz = Stats.getPlatz();
            top5Platz = Stats.getTop5();
            //  init();
            //      setTop5Wall();
           // print();
        }

      //  schedule();
    }

    private void init(){
        locationPlayer1 = new Location(Bukkit.getWorld("world"),112,122,62);
        locationPlayer2 = new Location(Bukkit.getWorld("world"),111,122,62);
        locationPlayer3 = new Location(Bukkit.getWorld("world"),110,122,62);
        locationPlayer4 = new Location(Bukkit.getWorld("world"),109,122,62);
        locationPlayer5 = new Location(Bukkit.getWorld("world"),108,122,62);
    }

    public void schedule(){
        if (plugin.getMySQL().isConnected()){
            Timer t = new Timer();
            t.schedule(new TimerTask(){

                @Override
                public void run() {
                   if (plugin.getMySQL().isConnected()){
                        allPlatz = Stats.getPlatz();
                        top5Platz = Stats.getTop5();
                 //       setTop5Wall();
                   } else {
                //       plugin.getMySQL().connect();
                   }
                }

            }, 0, 1000*60);
        }
    }

    public void setTop5Wall(){

        List<Location> locations = new ArrayList<>();
        locations.add(locationPlayer1);
        locations.add(locationPlayer2);
       /* locations.add(locationPlayer3);
        locations.add(locationPlayer4);
        locations.add(locationPlayer5);*/

        List<String> plätze = new ArrayList<>();
        String platz1 = "D151l";
        String platz2 = "D151l";
        String platz3 = "D151l";
        String platz4 = "D151l";
        String platz5 = "D151l";

        for (String name : top5Platz.keySet()){
            if (top5Platz.get(name) == 1){
                platz1 = name;
            }
            if (top5Platz.get(name) == 2){
                platz2 = name;
            }
            if (top5Platz.get(name) == 3){
                platz3 = name;
            }
            if (top5Platz.get(name) == 4){
                platz4 = name;
            }
            if (top5Platz.get(name) == 5){
                platz5 = name;
            }
        }

        plätze.add(platz1);
        plätze.add(platz2);
       /* plätze.add(platz3);
        plätze.add(platz4);
        plätze.add(platz5);*/

        for (int i = 0; i < locations.size() ; i++) {
            locations.get(i).getBlock().setType(Material.SKULL);
            Skull skull = (Skull) locations.get(i).getBlock().getState();
            skull.setSkullType(SkullType.PLAYER);
            skull.setOwner(plätze.get(i));
            Location locationSkull = new Location(locations.get(i).getWorld(),locations.get(i).getX(),locations.get(i).getY(),locations.get(i).getZ());

            Location signLocation = locationSkull.subtract(0,1,0);

            if (signLocation.getBlock().getState() instanceof Sign){
                BlockState blockState = signLocation.getBlock().getState();
                Sign sign = (Sign) blockState;
                sign.setLine(0,"§7Platz §8#§6"+top5Platz.get(plätze.get(i)));
                sign.setLine(1,"§6"+plätze.get(i));
            }
        }

    }

    public void print(){
        Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§7Top5:");
        for (String name : getTop5Platz().keySet()){
            Bukkit.getConsoleSender().sendMessage("§6"+name+"§7 Platz: §6"+getTop5Platz().get(name));
        }
    }

    public HashMap<String, Integer> getAllPlatz() {
        return allPlatz;
    }

    public HashMap<String, Integer> getTop5Platz() {
        return top5Platz;
    }
}
