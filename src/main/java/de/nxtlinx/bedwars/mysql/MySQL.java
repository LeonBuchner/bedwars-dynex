package de.nxtlinx.bedwars.mysql;

import de.nxtlinx.bedwars.BedWars;
import org.bukkit.Bukkit;

import java.sql.*;

public class MySQL {

    private String host = "";
    private String databse = "";
    private  String user = "";
    private String password = "";

    private Connection connection;

    public MySQL(String host,String database,String user,String password){
        this.host = host;
        this.databse = database;
        this.user = user;
        this.password = password;

        connect();
    }

    public MySQL(String host,String database,String user){
        this.host = host;
        this.databse = database;
        this.user = user;
        connect();
    }

    public MySQL(){
        this.host = FileManager.getHost();
        this.databse = FileManager.getDatabase();
        this.user = FileManager.getUsername();
        this.password = FileManager.getPassword();
        connect();
    }

    public void connect(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://"+host+":3306/"+databse+"?autoReconnect=true",user,password);
            Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§8[§9MySQL§8] §aEs wurde eine Verbindung hergestellt.");
            createTable();
        } catch (Exception e) {
            Bukkit.getConsoleSender().sendMessage( BedWars.PREFIX+"§8[§9MySQL§8] §4Es gab einen Fehler!");
            // e.printStackTrace();
        }

    }

    public void disconnect(){
        try {
            if (connection != null){
                connection.close();
                Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§8[§9MySQL§8] §aDie Verbindung wurde getrennt!");
            }
        } catch (SQLException ex){
            Bukkit.getConsoleSender().sendMessage( BedWars.PREFIX+"§8[§9MySQL§8] §4Es gab einen Fehler!");
            //  ex.printStackTrace();
        }
    }

    public void createTable(){
        try {
            connection.prepareStatement("CREATE TABLE IF NOT EXISTS bedwars (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, uuid VARCHAR(100), name VARCHAR(100), kills INT(16), deaths INT(16), winns INT(16), losses INT(16), played INT(16))").executeUpdate();
            Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§8[§9MySQL§8] §aEs wurde eine Tabelle erstellt.");
        } catch (SQLException e) {
            Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§8[§9MySQL§8] §4Es gab einen Fehler bei der erstellung der Tabelle!");
            // e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public boolean isConnected(){
        return (connection != null);
    }

    public void update(String qry){

        try {
            Statement st = connection.createStatement();
            st.executeUpdate(qry);
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePREPARE(String qry){

        try {
            PreparedStatement st = connection.prepareStatement(qry);
            st.executeUpdate(qry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet getResult(String qry){
        try {
            PreparedStatement st = connection.prepareStatement(qry);
            return st.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet query(String qry){
        ResultSet rs = null;
        try {
            Statement st = connection.createStatement();
            rs = st.executeQuery(qry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }
}
