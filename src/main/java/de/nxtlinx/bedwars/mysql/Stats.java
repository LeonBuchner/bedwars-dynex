package de.nxtlinx.bedwars.mysql;

import de.nxtlinx.bedwars.BedWars;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Stats {

    private static MySQL mySQL = BedWars.getPlugin().getMySQL();


    public static boolean playerExists(String uuid){
        try {
            ResultSet rs = mySQL.query("SELECT * FROM bedwars WHERE uuid= '"+uuid+"'");
            if (rs.next()){
                return rs.getString("uuid") != null;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void createPlayer(String uuid,String name){
        if (!playerExists(uuid)){
            mySQL.update("INSERT INTO bedwars (uuid,name,kills,deaths,winns,losses,played) VALUES ('"+uuid+"','"+name+"','0','0','0','0','0');");
        }
    }

    public static boolean playerExistsByName(String name){
        try {
            ResultSet rs = mySQL.query("SELECT * FROM bedwars WHERE name= '"+name+"'");
            if (rs.next()){
                return rs.getString("name") != null;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static int getKills(String uuid){
        Integer i = 0;
        if (playerExists(uuid)) {
            try {
                ResultSet rs = mySQL.query("SELECT kills FROM bedwars WHERE uuid = '"+uuid+"'");
                if ((!rs.next()) || (Integer.valueOf(rs.getInt("kills")) == null));
                i = rs.getInt("kills");
            } catch (SQLException ex){
                ex.printStackTrace();
            }

        }
        return i;
    }

    public static void setKills(String uuid,int kills){
        if (playerExists(uuid)) {
            if (getKills(uuid) == 0) {
                //  mySQL.update("INSERT INTO coinsTable (uuid,coins) VALUES ('"+uuid+"','"+coins+"')");
                mySQL.update("UPDATE bedwars SET kills = '"+kills+"' WHERE uuid = '"+uuid+"';");
            } else {
                mySQL.update("UPDATE bedwars SET kills = '"+kills+"' WHERE uuid = '"+uuid+"';");
            }
        }
    }



    public static void addKills(String uuid,int kills){
        if (playerExists(uuid)) {
            setKills(uuid, kills + getKills(uuid));
        }
    }

    public static void removeKills(String uuid,int kills){
        if (playerExists(uuid)) {
            setKills(uuid, getKills(uuid) - kills);
        }
    }

    public static void removeAllKills(String uuid){
        if (playerExists(uuid)) {
            setKills(uuid, 0);
        }
    }



    public static int getDeaths(String uuid){
        Integer i = 0;
        if (playerExists(uuid)) {
            try {
                ResultSet rs = mySQL.query("SELECT deaths FROM bedwars WHERE uuid = '"+uuid+"'");
                if ((!rs.next()) || (Integer.valueOf(rs.getInt("deaths")) == null));
                i = rs.getInt("deaths");
            } catch (SQLException ex){
                ex.printStackTrace();
            }

        }
        return i;
    }

    public static void setDeaths(String uuid,int deaths){
        if (playerExists(uuid)) {
            if (getDeaths(uuid) == 0) {
                //  mySQL.update("INSERT INTO coinsTable (uuid,coins) VALUES ('"+uuid+"','"+coins+"')");
                mySQL.update("UPDATE bedwars SET deaths = '"+deaths+"' WHERE uuid = '"+uuid+"';");
            } else {
                mySQL.update("UPDATE bedwars SET deaths = '"+deaths+"' WHERE uuid = '"+uuid+"';");
            }
        }
    }



    public static void addDeaths(String uuid,int deaths){
        if (playerExists(uuid)) {
            setDeaths(uuid, deaths + getDeaths(uuid));
        }
    }

    public static void removeDeaths(String uuid,int deaths){
        if (playerExists(uuid)) {
            setDeaths(uuid, getDeaths(uuid) - deaths);
        }
    }

    public static void removeAllDeaths(String uuid){
        if (playerExists(uuid)) {
            setDeaths(uuid, 0);
        }
    }

    public static int getWinns(String uuid){
        Integer i = 0;
        if (playerExists(uuid)) {
            try {
                ResultSet rs = mySQL.query("SELECT winns FROM bedwars WHERE uuid = '"+uuid+"'");
                if ((!rs.next()) || (Integer.valueOf(rs.getInt("winns")) == null));
                i = rs.getInt("winns");
            } catch (SQLException ex){
                ex.printStackTrace();
            }

        }
        return i;
    }

    public static void setWinns(String uuid,int winns){
        if (playerExists(uuid)) {
            if (getWinns(uuid) == 0) {
                //  mySQL.update("INSERT INTO coinsTable (uuid,coins) VALUES ('"+uuid+"','"+coins+"')");
                mySQL.update("UPDATE bedwars SET winns = '"+winns+"' WHERE uuid = '"+uuid+"';");
            } else {
                mySQL.update("UPDATE bedwars SET winns = '"+winns+"' WHERE uuid = '"+uuid+"';");
            }
        }
    }



    public static void addWinns(String uuid,int winns){
        if (playerExists(uuid)) {
            setWinns(uuid, winns + getWinns(uuid));
        }
    }

    public static void removeWinns(String uuid,int winns){
        if (playerExists(uuid)) {
            setWinns(uuid, getWinns(uuid) - winns);
        }
    }

    public static void removeAllWins(String uuid){
        if (playerExists(uuid)) {
            setWinns(uuid, 0);
        }
    }

    //  losses


    public static int getLosses(String uuid){
        Integer i = 0;
        if (playerExists(uuid)) {
            try {
                ResultSet rs = mySQL.query("SELECT losses FROM bedwars WHERE uuid = '"+uuid+"'");
                if ((!rs.next()) || (Integer.valueOf(rs.getInt("losses")) == null));
                i = rs.getInt("losses");
            } catch (SQLException ex){
                ex.printStackTrace();
            }

        }
        return i;
    }

    public static void setLosses(String uuid,int losses){
        if (playerExists(uuid)) {
            if (getLosses(uuid) == 0) {
                //  mySQL.update("INSERT INTO coinsTable (uuid,coins) VALUES ('"+uuid+"','"+coins+"')");
                mySQL.update("UPDATE bedwars SET losses = '"+losses+"' WHERE uuid = '"+uuid+"';");
            } else {
                mySQL.update("UPDATE bedwars SET losses = '"+losses+"' WHERE uuid = '"+uuid+"';");
            }
        }
    }



    public static void addLosses(String uuid,int losses){
        if (playerExists(uuid)) {
            setLosses(uuid, losses + getLosses(uuid));
        }
    }

    public static void removeLosses(String uuid,int losses){
        if (playerExists(uuid)) {
            setLosses(uuid, getLosses(uuid) - losses);
        }
    }

    public static void removeAllLosses(String uuid){
        if (playerExists(uuid)) {
            setLosses(uuid, 0);
        }
    }

    // played

    public static int getPlayed(String uuid){
        Integer i = 0;
        if (playerExists(uuid)) {
            try {
                ResultSet rs = mySQL.query("SELECT played FROM bedwars WHERE uuid = '"+uuid+"'");
                if ((!rs.next()) || (Integer.valueOf(rs.getInt("played")) == null));
                i = rs.getInt("played");
            } catch (SQLException ex){
                ex.printStackTrace();
            }

        }
        return i;
    }

    public static void setPlayed(String uuid,int played){
        if (playerExists(uuid)) {
            if (getPlayed(uuid) == 0) {
                //  mySQL.update("INSERT INTO coinsTable (uuid,coins) VALUES ('"+uuid+"','"+coins+"')");
                mySQL.update("UPDATE bedwars SET played = '"+played+"' WHERE uuid = '"+uuid+"';");
            } else {
                mySQL.update("UPDATE bedwars SET played = '"+played+"' WHERE uuid = '"+uuid+"';");
            }
        }
    }

    public static void setName(String uuid,String name){
        if (playerExists(uuid)) {
            mySQL.update("UPDATE bedwars SET name = '"+name+"' WHERE uuid = '"+uuid+"';");
        }
    }

    public static String getName(String uuid){
       String string = "noname";
        if (playerExists(uuid)) {
            try {
                ResultSet rs = mySQL.query("SELECT name FROM bedwars WHERE uuid = '"+uuid+"'");
                if ((!rs.next()) || ((rs.getString("name")) == null));
                string = rs.getString("name");
            } catch (SQLException ex){
                ex.printStackTrace();
            }

        }
        return string;
    }

    public static String getUUID(String name){
        String string = "noname";
        if (playerExistsByName(name)) {
            try {
                ResultSet rs = mySQL.query("SELECT uuid FROM bedwars WHERE name = '"+name+"'");
                if ((!rs.next()) || ((rs.getString("uuid")) == null));
                string = rs.getString("uuid");
            } catch (SQLException ex){
                ex.printStackTrace();
            }

        }
        return string;
    }



    public static void addPlayed(String uuid,int played){
        if (playerExists(uuid)) {
            setPlayed(uuid, played + getPlayed(uuid));
        }
    }

    public static void removePlayed(String uuid,int played){
        if (playerExists(uuid)) {
            setPlayed(uuid, getPlayed(uuid) - played);
        }
    }

    public static void removeAllPlayed(String uuid){
        if (playerExists(uuid)) {
            setPlayed(uuid, 0);
        }
    }

    public static HashMap<String,Integer> getPlatz(){
        HashMap<String,Integer> place = new HashMap<>();
        try {
            int in = 0;
            ResultSet rs = mySQL.query("SELECT uuid FROM bedwars ORDER BY kills DESC");
            while (rs.next()){
                in++;
                place.put(getName(rs.getString("uuid")),in);
            }
        } catch (SQLException ex){
            ex.printStackTrace();
        }
        return place;
    }

    public static HashMap<String,Integer> getTop5(){
        HashMap<String,Integer> place = new HashMap<>();
        try {
            int in = 0;
            ResultSet rs = mySQL.query("SELECT uuid FROM bedwars ORDER BY kills DESC LIMIT 5");
            while (rs.next()){
                in++;
                place.put(getName(rs.getString("uuid")),in);
            }
        } catch (SQLException ex){
            ex.printStackTrace();
        }
        return place;
    }

    public static void removeAllStats(String uuid){
        if (playerExists(uuid)){
            removeAllPlayed(uuid);
            removeAllLosses(uuid);
            removeAllWins(uuid);
            removeAllDeaths(uuid);
            removeAllKills(uuid);
        }

    }
    
}
