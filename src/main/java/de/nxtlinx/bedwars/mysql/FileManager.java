package de.nxtlinx.bedwars.mysql;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileManager {

    public static File file = new File("plugins/MySQL/BedWars", "mysql.yml");
    public static FileConfiguration cn = YamlConfiguration.loadConfiguration(file);

    private static String PREFIX;
    private static String host;
    private static String username;
    private static String password;
    private static String database;


    public static void setConfig(){

        cn.set("MySQL.Host","localhost");
        cn.set("MySQL.User","root");
        cn.set("MySQL.Password","test");
        cn.set("MySQL.Database","stats");

        cn.options().copyDefaults(true);

        try {
            cn.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadInfo(){

        host = cn.getString("MySQL.Host");
        username = cn.getString("MySQL.User");
        password = cn.getString("MySQL.Password");
        database = cn.getString("MySQL.Database");

        saveConfig();


    }


    public static void saveConfig(){
        try {
            cn.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean fileExists(){
        return file.exists();
    }

    public static String getHost() {
        return host;
    }

    public static String getUsername() {
        return username;
    }

    public static String getDatabase() {
        return database;
    }

    public static String getPassword() {
        return password;
    }

}
