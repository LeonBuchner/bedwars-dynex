package de.nxtlinx.bedwars.setup;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.teams.Teams;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class BedUtil {

    private BedWars plugin;
    
    private File file;
    private FileConfiguration cfg;
    
    public BedUtil(BedWars plugin){
        this.plugin = plugin;
        file = new File("plugins/BedWars/Bed","location.yml");
        cfg = YamlConfiguration.loadConfiguration(file);
    }

    private void saveConfig(){
        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setBlockLocation1(Block location, Teams teams){
        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        String w = location.getWorld().getName();

        cfg.set("Block.X."+teams+".1", x);
        cfg.set("Block.Y."+teams+".1", y);
        cfg.set("Block.Z."+teams+".1", z);
        cfg.set("Block.W."+teams+".1", w);
        saveConfig();
    }

    public void setBlockLocation2(Block location, Teams teams){
        double x = location.getX();
        double y = location.getY();
        double z = location.getZ();
        String w = location.getWorld().getName();

        cfg.set("Block.X."+teams+".2", x);
        cfg.set("Block.Y."+teams+".2", y);
        cfg.set("Block.Z."+teams+".2", z);
        cfg.set("Block.W."+teams+".2", w);
        saveConfig();
    }


    public Block getBlockLocation1(Teams teams){
        double x = (double) cfg.getDouble("Block.X."+teams+".1");
        double y = (double) cfg.getDouble("Block.Y."+teams+".1");
        double z = (double) cfg.getDouble("Block.Z."+teams+".1");
        String w = (String) cfg.getString("Block.W."+teams+".1");

        World world = Bukkit.getWorld(w);
        return new Location(world, x, y, z).getBlock();
    }

    public Block getBlockLocation2(Teams teams){
        double x = (double) cfg.getDouble("Block.X."+teams+".2");
        double y = (double) cfg.getDouble("Block.Y."+teams+".2");
        double z = (double) cfg.getDouble("Block.Z."+teams+".2");
        String w = (String) cfg.getString("Block.W."+teams+".2");

        World world = Bukkit.getWorld(w);
        return new Location(world, x, y, z).getBlock();
    }

    public FileConfiguration getCfg() {
        return cfg;
    }

    public File getFile() {
        return file;
    }
}
