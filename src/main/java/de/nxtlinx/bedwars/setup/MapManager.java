package de.nxtlinx.bedwars.setup;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.teams.Teams;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MapManager {

    private File file;
    private FileConfiguration configuration;

    private List<String> maps;
    private ArrayList<String> strings;

    public MapManager(){
        file = new File("plugins/BedWars/Map","maps.yml");
        configuration = YamlConfiguration.loadConfiguration(file);
        strings = new ArrayList<String>();
    }

    public void setLobby(Location location){

        configuration.set("Lobby.X",location.getX());
        configuration.set("Lobby.Y",location.getY());
        configuration.set("Lobby.Z",location.getZ());
        configuration.set("Lobby.World",location.getWorld().getName());
        configuration.set("Lobby.Yaw",location.getYaw());
        configuration.set("Lobby.Pitch",location.getPitch());

        try {
            configuration.save(file);
        }catch (IOException e){
            Bukkit.getConsoleSender().sendMessage("§cFehler");
        }

    }

    public void createMap(String name, Location location, Teams players){

        if (configuration.getStringList("Maps") == null){
            configuration.set("Maps",strings);
            try {
                configuration.save(file);
            }catch (IOException e){
                Bukkit.getConsoleSender().sendMessage("§cFehler");
            }
        }

        maps = configuration.getStringList("Maps");
        if (!maps.contains(name)){
            //   return;
            maps.add(name);
            configuration.set("Maps",maps);
            int i = configuration.getInt("Map."+name+".size");
            configuration.set("Map."+name+".size",i+1);
            // configuration.set("Map."+name+".size",1);

        } /*else {

        }*/

        configuration.set("Map."+name+".X."+players.name(),location.getX());
        configuration.set("Map."+name+".Y."+players.name(),location.getY());
        configuration.set("Map."+name+".Z."+players.name(),location.getZ());
        configuration.set("Map."+name+".World."+players.name(),location.getWorld().getName());
        configuration.set("Map."+name+".Yaw."+players.name(),location.getYaw());
        configuration.set("Map."+name+".Pitch."+players.name(),location.getPitch());

        try {
            configuration.save(file);
        }catch (IOException e){
            Bukkit.getConsoleSender().sendMessage("§cFehler");
        }

    }

    public Location getMapLocation(String name,Teams players){
        double x = configuration.getDouble("Map."+name+".X."+players.name());
        double y = configuration.getDouble("Map."+name+".Y."+players.name());
        double z = configuration.getDouble("Map."+name+".Z."+players.name());
        String w = configuration.getString("Map."+name+".World."+players.name());
        float yaw = (float) configuration.getDouble("Map."+name+".Yaw."+players.name());
        float pitch = (float) configuration.getDouble("Map."+name+".Pitch."+players.name());

        World world = Bukkit.getWorld(w);
        return new Location(world,x,y,z,yaw,pitch);
    }

    public Block getMapSpawnBlock(String name,Teams players){
        double x = configuration.getDouble("Map."+name+".X."+players.name());
        double y = configuration.getDouble("Map."+name+".Y."+players.name());
        double z = configuration.getDouble("Map."+name+".Z."+players.name());
        String w = configuration.getString("Map."+name+".World."+players.name());

        World world = Bukkit.getWorld(w);
        return new Location(world,x,y,z).getBlock();
    }

    public void createSpec(Location location,String name){

        configuration.set("Spec."+name+".X",location.getX());
        configuration.set("Spec."+name+".Y",location.getY());
        configuration.set("Spec."+name+".Z",location.getZ());
        configuration.set("Spec."+name+".World",location.getWorld().getName());
        configuration.set("Spec."+name+".Yaw",location.getYaw());
        configuration.set("Spec."+name+".Pitch",location.getPitch());

        try {
            configuration.save(file);
        }catch (IOException e){
            Bukkit.getConsoleSender().sendMessage("§cFehler");
        }

    }

    public String getRandomMap(){
        String str = "Error";
        if (configuration.getStringList("Maps") == null){
            configuration.set("Maps", new ArrayList<String>());
            try {
                configuration.save(file);
            }catch (IOException e){
                Bukkit.getConsoleSender().sendMessage("§cFehler");
            }
            return str;
        } else {
            List<String> maps = configuration.getStringList("Maps");
            Random random = new Random();
            str = maps.get(random.nextInt(maps.size()));
        }
        return str;
    }

    public void createBed1(Location location,Teams teams,String mapName){
        configuration.set("Bed."+teams.name()+"."+mapName+".X.1",location.getX());
        configuration.set("Bed."+teams.name()+"."+mapName+".Y.1",location.getY());
        configuration.set("Bed."+teams.name()+"."+mapName+".Z.1",location.getZ());
        configuration.set("Bed."+teams.name()+"."+mapName+".World.1",location.getWorld().getName());
        try {
            configuration.save(file);
        }catch (IOException e){
            Bukkit.getConsoleSender().sendMessage("§cFehler");
        }
    }

    public void createBed2(Location location,Teams teams,String mapName){
        configuration.set("Bed."+teams.name()+"."+mapName+".X.2",location.getX());
        configuration.set("Bed."+teams.name()+"."+mapName+".Y.2",location.getY());
        configuration.set("Bed."+teams.name()+"."+mapName+".Z.2",location.getZ());
        configuration.set("Bed."+teams.name()+"."+mapName+".World.2",location.getWorld().getName());
        try {
            configuration.save(file);
        }catch (IOException e){
            Bukkit.getConsoleSender().sendMessage("§cFehler");
        }
    }

    public static Teams containsBedLocation(Block block,String map){
        MapManager mapManager = new MapManager();
       for (Teams teams : BedWars.getPlugin().getAllTeams()){
           if (block.getLocation().equals(mapManager.getBedLocation1(teams,map))){
               return teams;
           }
           if (block.getLocation().equals(mapManager.getBedLocation2(teams,map))){
               return teams;
           }
       }
       return null;
    }

    public void setDeathHigh(Location location,String map){
        configuration.set("Map."+map+".Death",location.getY());
        try {
            configuration.save(file);
        }catch (IOException e){
            Bukkit.getConsoleSender().sendMessage("§cFehler");
        }
    }

    public double getDeathHigh(String map){
       return configuration.getDouble("Map."+map+".Death");
    }


    public Block getBed1(Teams teams,String map){
        double x = configuration.getDouble("Bed."+teams.name()+"."+map+".X.1");
        double y = configuration.getDouble("Bed."+teams.name()+"."+map+".Y.1");
        double z = configuration.getDouble("Bed."+teams.name()+"."+map+".Z.1");
        String w = configuration.getString("Bed."+teams.name()+"."+map+".World.1");

        World world = Bukkit.getWorld(w);
        return new Location(world,x,y,z).getBlock();
    }

    public Block getBed2(Teams teams,String map){
        double x = configuration.getDouble("Bed."+teams.name()+"."+map+".X.2");
        double y = configuration.getDouble("Bed."+teams.name()+"."+map+".Y.2");
        double z = configuration.getDouble("Bed."+teams.name()+"."+map+".Z.2");
        String w = configuration.getString("Bed."+teams.name()+"."+map+".World.2");

        World world = Bukkit.getWorld(w);
        return new Location(world,x,y,z).getBlock();
    }

    public Location getBedLocation1(Teams teams,String map){
        double x = configuration.getDouble("Bed."+teams.name()+"."+map+".X.1");
        double y = configuration.getDouble("Bed."+teams.name()+"."+map+".Y.1");
        double z = configuration.getDouble("Bed."+teams.name()+"."+map+".Z.1");
        String w = configuration.getString("Bed."+teams.name()+"."+map+".World.1");

        World world = Bukkit.getWorld(w);
        return new Location(world,x,y,z);
    }

    public Location getBedLocation2(Teams teams,String map){
        double x = configuration.getDouble("Bed."+teams.name()+"."+map+".X.2");
        double y = configuration.getDouble("Bed."+teams.name()+"."+map+".Y.2");
        double z = configuration.getDouble("Bed."+teams.name()+"."+map+".Z.2");
        String w = configuration.getString("Bed."+teams.name()+"."+map+".World.2");

        World world = Bukkit.getWorld(w);
        return new Location(world,x,y,z);
    }




    public Location getSpec(String name){

        double x = configuration.getDouble("Spec."+name+".X");
        double y = configuration.getDouble("Spec."+name+".Y");
        double z = configuration.getDouble("Spec."+name+".Z");
        String w = configuration.getString("Spec."+name+".World");
        float yaw = (float) configuration.getDouble("Spec."+name+".Yaw");
        float pitch = (float) configuration.getDouble("Spec."+name+".Pitch");

        World world = Bukkit.getWorld(w);
        return new Location(world,x,y,z,yaw,pitch);

    }

    public String getBuilder(String mapname){
        if (configuration.getString("Map."+mapname+".Build") != null){
            return configuration.getString("Map."+mapname+".Build");
        }
        return "DynexMC-Builder Team";
    }

    public boolean setBuilder(String mapname,String builder){
        if (configuration.getStringList("Maps").contains(mapname)){
            configuration.set("Map."+mapname+".Build",builder);
            try {
                configuration.save(file);
            }catch (IOException e){
                Bukkit.getConsoleSender().sendMessage("§cFehler");
            }
            return true;
        }
        return false;
    }

    public boolean mapExists(String mapname){
        return configuration.getStringList("Maps").contains(mapname);
    }

    public Location getLobby(){
        double x = configuration.getDouble("Lobby.X");
        double y = configuration.getDouble("Lobby.Y");
        double z = configuration.getDouble("Lobby.Z");
        String w = configuration.getString("Lobby.World");
        float yaw = (float) configuration.getDouble("Lobby.Yaw");
        float pitch = (float) configuration.getDouble("Lobby.Pitch");

        World world = Bukkit.getWorld(w);
        return new Location(world,x,y,z,yaw,pitch);
    }

    public File getFile() {
        return file;
    }

    public FileConfiguration getconfiguration() {
        return configuration;
    }
    
}
