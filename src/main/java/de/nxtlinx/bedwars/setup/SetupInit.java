package de.nxtlinx.bedwars.setup;

import de.nxtlinx.bedwars.teams.Teams;

public class SetupInit {

    private Teams teams;
    private String map;

    public SetupInit() {}

    public SetupInit(Teams teams,String map){
        this.teams = teams;
        this.map = map;
    }

    public Teams getTeams() {
        return teams;
    }

    public String getMap() {
        return map;
    }
}
