package de.nxtlinx.bedwars.setup;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.teams.Item;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;

public class ItemManager {

    private BedWars plugin;

    public ItemManager(BedWars plugin){
        this.plugin = plugin;
    }

    public Integer setSpawn(Item type, Location location,String mapName){
        int i = 0;

        File file = new File("plugins/BedWars/Spawner","spawner.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        i = cfg.getInt(type+".amount."+mapName);
        i++;
        cfg.set(type+".amount."+mapName,i);

        cfg.set(type+"."+mapName+"."+i+".world",location.getWorld().getName());
        cfg.set(type+"."+mapName+"."+i+".x",location.getX());
        cfg.set(type+"."+mapName+"."+i+".y",location.getY());
        cfg.set(type+"."+mapName+"."+i+".z",location.getZ());
        save(file,cfg);

        return i;
    }

    public Integer setShop(Location location, String mapName){
        int i = 0;
        File file = new File("plugins/BedWars/Shop","shop.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        i = cfg.getInt("Shop."+mapName+".amount");
        i++;
        cfg.set("Shop."+mapName+".amount",i);

        cfg.set("Shop."+mapName+"."+i+".world",location.getWorld().getName());
        cfg.set("Shop."+mapName+"."+i+".x",location.getX());
        cfg.set("Shop."+mapName+"."+i+".y",location.getY());
        cfg.set("Shop."+mapName+"."+i+".z",location.getZ());
        save(file,cfg);

        return i;
    }

    public void spawnShop(){
        String mapName = plugin.MAP;
        File file = new File("plugins/BedWars/Shop","shop.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        if (file.exists()){

            int amount = cfg.getInt("Shop."+mapName+".amount");

            for (int i = 1; i < amount+1 ; i++) {
                String world = cfg.getString("Shop."+mapName+"."+ i + ".world");
                double x = cfg.getDouble("Shop."+mapName+"."+ i + ".x");
                double y = cfg.getDouble("Shop."+mapName+"."+ i + ".y");
                double z = cfg.getDouble("Shop."+mapName+"."+ i + ".z");

                Location location = new Location(Bukkit.getWorld(world), x, y, z);


                ArmorStand shop = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
                shop.setCustomName("§6Shop");
                shop.setCustomNameVisible(true);
            }
        } else {
            Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§cEs konnten keine Spawn-Shops spawnen");
        }
    }

    private void save(File file, FileConfiguration cfg){
        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void spawn(Item type, ItemStack item,String mapName){
        File file = new File("plugins/BedWars/Spawner","spawner.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        int amount = cfg.getInt(type+".amount."+mapName);
     //   Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§7Spawn: §6"+type.name()+"§7 [§9"+mapName+"§7] §5Size: §6"+amount);

        for (int i = 1 ; i < amount+1 ; i++) {
            String world = cfg.getString(type+"."+mapName+"."+i+".world");
            double x = cfg.getDouble(type+"."+mapName+"."+i+".x");
            double y = cfg.getDouble(type+"."+mapName+"."+i+".y");
            double z = cfg.getDouble(type+"."+mapName+"."+i+".z");

            World w = Bukkit.getWorld(world);
            Location location = new Location(w,x,y,z);

          //  location.getWorld().dropItemNaturally(location,item);
            location.getWorld().dropItem(location,item);
      //      Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§7Spawn: §6"+type.name()+"§7 [§9"+mapName+"§7]");
        }
    }

    public void startSpawners(){
        final String map = plugin.MAP;
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            public void run() {
                ItemStack item = new ItemStack(Material.CLAY_BRICK);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§7Bronze");
                item.setItemMeta(meta);

                spawn(Item.BRONZE,item,map);
            }
        },20*1,20*1);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            public void run() {
                ItemStack item = new ItemStack(Material.IRON_INGOT);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName("§7Eisen");
                item.setItemMeta(meta);

                spawn(Item.EISEN,item,map);
            }
        },20*10,20*10);

        if (plugin.isGold()){
            Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
                public void run() {
                    ItemStack item = new ItemStack(Material.GOLD_INGOT);
                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName("§7Gold");
                    item.setItemMeta(meta);

                    spawn(Item.GOLD,item,map);
                }
            },20*30,20*30);
        }
    }

    public void cancelSpawning(){
        Bukkit.getScheduler().cancelAllTasks();
    }
}
