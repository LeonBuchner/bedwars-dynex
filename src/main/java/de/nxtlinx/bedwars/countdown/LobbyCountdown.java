package de.nxtlinx.bedwars.countdown;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.gamemanager.GameManager;
import de.nxtlinx.bedwars.gamemanager.GameState;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class LobbyCountdown extends Countdown{

    private BedWars plugin;

    private GameManager gameManager;

    private final int RESET_SECONDS = 10;
    private final int IDLE_SECONDS = 20;

    private int idleID;
    private boolean isRunning = false, isIdling = false, isStarting = false;
    private int seconds = 60;

    public LobbyCountdown(BedWars plugin){
        this.plugin = plugin;
        this.gameManager = plugin.getGameManager();
    }


    @Override
    public void run() {
        isRunning = true;

        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(BedWars.getPlugin(), new Runnable() {

            public void run() {

                switch (seconds) {
                    case 60: case 30: case 15: case 12: case 11: case 10: case 5: case 4: case 3: case 2:
                        Bukkit.broadcastMessage(BedWars.PREFIX + "§aDas Spiel startet in §6" + seconds + "§a Sekunden§a!");
                        if(seconds == 11) {
                            isStarting = true;
                        }
                        if (seconds == 5) {
                            plugin.setForceMap(false);
                            plugin.setGoldVote(false);
                            plugin.setMapVote(false);
                        }
                        if (seconds == 3) {
                            // NO-Voting
                            String map = plugin.getVoteManager().getWinnnerMap();
                            if (!map.equals("NO-Voting")){
                                BedWars.MAP = map;
                                Bukkit.broadcastMessage(BedWars.PREFIX + "§7Es wurde die Map §6" + BedWars.MAP+" §7gewählt");
                            } else {
                                Bukkit.broadcastMessage(BedWars.PREFIX + "§7Map: §6" + BedWars.MAP);
                            }
                            boolean gold = plugin.getVoteManager().getWinnnerGold();
                            plugin.setGold(gold);
                            if (gold){
                                Bukkit.broadcastMessage(BedWars.PREFIX + "§6Gold §7wurde §aaktiviert");
                            } else {
                                Bukkit.broadcastMessage(BedWars.PREFIX + "§6Gold §7wurde §4deaktiviert");
                            }
                        }
                        break;
                    case 1:
                        Bukkit.broadcastMessage(BedWars.PREFIX + "§aDas Spiel startet in §6einer §aSekunde§a!");
                        gameManager.startIngame();
                        break;
                    case 0:
                       gameManager.setGameState(GameState.INGAME);
                        for (Player all : Bukkit.getOnlinePlayers()){
                            all.setGameMode(GameMode.SURVIVAL);
                            all.setLevel(0);
                        }

                        break;

                    default:
                        break;
                }

                for (Player all : Bukkit.getOnlinePlayers()){
                    all.setLevel(seconds);
                }

                seconds--;
            }
        }, 0, 1 * 20);
    }

    @Override
    public void cancel() {
        isRunning = false;
        isStarting = false;

        Bukkit.getScheduler().cancelTask(taskID);
        seconds = RESET_SECONDS;

    }

    public void idle() {

        isIdling = true;

        idleID = Bukkit.getScheduler().scheduleSyncRepeatingTask(BedWars.getPlugin(), new Runnable() {

            public void run() {
                int missingPlayers = GameManager.MIN_PLAYERS - gameManager.getPlayers().size();

                if(missingPlayers != 1) {
                    Bukkit.broadcastMessage(BedWars.PREFIX + "§7Es fehlen noch §6" + missingPlayers + " Spieler §7bis zum Start.");
                } else
                    Bukkit.broadcastMessage(BedWars.PREFIX + "§7Es fehlt noch §6ein Spieler §7bis zum Start.");

            }
        }, IDLE_SECONDS * 20, IDLE_SECONDS * 20);
    }

    public void cancleIdle() {
        isIdling = false;

        Bukkit.getScheduler().cancelTask(idleID);
    }

    public boolean isRunning() {
        return isRunning;
    }

    public boolean isIdling() {
        return isIdling;
    }

    public void setStarting(boolean isStarting) {
        this.isStarting = isStarting;
    }

    public boolean isStarting() {
        return isStarting;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getSeconds() {
        return seconds;
    }

}
