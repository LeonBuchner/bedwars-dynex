package de.nxtlinx.bedwars.countdown;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.gamemanager.GameManager;
import de.nxtlinx.bedwars.teams.TeamManager;
import de.nxtlinx.bedwars.teams.Teams;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

public class EndCountdown extends Countdown {

    private BedWars plugin;
    private int seconds = 10;

    public EndCountdown(BedWars plugin){
        this.plugin = plugin;
    }


    @Override
    public void run() {

        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(BedWars.getPlugin(), new Runnable() {

            public void run() {

                switch (seconds) {
                    case 60: case 30: case 15: case 10: case 5: case 4: case 3: case 2:
                        Bukkit.broadcastMessage(BedWars.PREFIX + "§cDer Server stoppt in §6" + seconds + " §6Sekunden§a!");
                        break;
                    case 1:
                        Bukkit.broadcastMessage(BedWars.PREFIX + "§a§cDer Server stoppt in §6einer §6Sekunde§a!");
                        break;
                    case 0:
                        for (Player all: Bukkit.getOnlinePlayers()){
                            all.kickPlayer(BedWars.PREFIX + "§cDas Spiel wurde gestoppt!");
                        }
                        if (plugin.isCloudNet()){
                            Bukkit.shutdown();
                        }
                        Bukkit.reload();
                        cancel();
                        break;

                    default:
                        break;
                }

                    Teams winnerTeam = plugin.getGameManager().getTeamsWinner();
                    if (winnerTeam != null){
                        for (Player teamPlayers : winnerTeam.getTeamPlayers()){
                            if (teamPlayers != null){
                                Firework firework = (Firework) teamPlayers.getLocation().getWorld().spawnEntity(teamPlayers.getLocation(), EntityType.FIREWORK);
                                FireworkMeta fireworkEffectMeta = firework.getFireworkMeta();
                                fireworkEffectMeta.addEffect(FireworkEffect.builder()
                                        .withColor(translateChatColor(teamPlayers))
                                        .flicker(true)
                                        .trail(true)
                                        .with(FireworkEffect.Type.BALL_LARGE)
                                        .build());
                                fireworkEffectMeta.setPower(1);
                                firework.setFireworkMeta(fireworkEffectMeta);
                            }
                        }
                    }

                seconds--;
            }
        }, 0, 1 * 20);
    }

    @Override
    public void cancel() {
        Bukkit.getScheduler().cancelTask(taskID);
    }

    private Color translateChatColor(Player player) {
        Color color = Color.GRAY;
        TeamManager teamManager = plugin.getTeamManager();
        Teams teams = teamManager.getPlayerTeam(player);
        if (teams == null){
            return color;
        }
        if (teams.getChatColor() == ChatColor.RED) {
            color = Color.RED;
        }
        if (teams.getChatColor() == ChatColor.GREEN) {
            color = Color.GREEN;
        }
        if (teams.getChatColor() == ChatColor.BLUE) {
            color = Color.BLUE;
        }
        if (teams.getChatColor() == ChatColor.YELLOW) {
            color = Color.YELLOW;
        }
        if (teams.getChatColor() == ChatColor.BLACK) {
            color = Color.BLACK;
        }
        if (teams.getChatColor() == ChatColor.DARK_PURPLE) {
            color = Color.PURPLE;
        }
        if (teams.getChatColor() == ChatColor.LIGHT_PURPLE) {
            color = Color.fromRGB(255, 192, 203);
        }
        if (teams.getChatColor() == ChatColor.GOLD) {
            color = Color.ORANGE;
        }
        return color;
    }
}
