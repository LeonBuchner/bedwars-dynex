package de.nxtlinx.bedwars.countdown;

import de.nxtlinx.bedwars.BedWars;

public class CountdownManager {

    private BedWars plugin;
    private LobbyCountdown lobbyCountdown;
    private EndCountdown endCountdown;

    public CountdownManager(BedWars plugin){
        this.plugin = plugin;
        this.lobbyCountdown = new LobbyCountdown(plugin);
        this.endCountdown = new EndCountdown(plugin);
    }


    public LobbyCountdown getLobbyCountdown() {
        return lobbyCountdown;
    }

    public EndCountdown getEndCountdown() {
        return endCountdown;
    }
}
