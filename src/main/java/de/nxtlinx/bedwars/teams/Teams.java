package de.nxtlinx.bedwars.teams;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public enum  Teams {

    RED("Rot",ChatColor.RED,(short) 14),
    GREEN("Grün",ChatColor.GREEN,(short)13),
    BLUE("Blau",ChatColor.BLUE,(short)11),
    YELLOW("Gelb",ChatColor.YELLOW,(short)4),
    BLACK("Schwarz",ChatColor.BLACK,(short)15),
    PURPLE("Lila",ChatColor.DARK_PURPLE,(short)10),
    LIGHT_PURPLE("Pink",ChatColor.LIGHT_PURPLE,(short)6),
    ORANGE("Orange",ChatColor.GOLD,(short)1);

    private String teamName;
    private ChatColor chatColor;
    private short colorID;
    private ArrayList<Player> teamPlayers;
    private boolean friendlyfire = true;


    private Teams(String teamName, ChatColor chatColor, short colorID){
        this.teamName = teamName;
        this.chatColor = chatColor;
        this.colorID = colorID;
        teamPlayers = new ArrayList<Player>();
    }

    public String getTeamName() {
        return teamName;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public short getColorID() {
        return colorID;
    }

    public ArrayList<Player> getTeamPlayers() {
        return teamPlayers;
    }

    public boolean isFriendlyfire() {
        return friendlyfire;
    }

    public String getPREFIX(){
        return chatColor+teamName;
    }
}
