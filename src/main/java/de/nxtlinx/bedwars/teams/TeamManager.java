package de.nxtlinx.bedwars.teams;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.gamemanager.GameManager;
import de.nxtlinx.bedwars.gamemanager.GameState;
import de.nxtlinx.bedwars.utils.ItemBuilder;
import de.nxtlinx.bedwars.utils.ScoreUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TeamManager implements Listener {
                                                    // 2
    public static final int MAY_BALANCE_OFFSET = GameManager.MAX_PLAYERS_PER_TEAM;

    private HashMap<Player, Teams> teamConnector;

    private BedWars plugin;

    public TeamManager(BedWars plugin) {
        this.plugin = plugin;
        teamConnector = new HashMap<>();
        this.plugin.getServer().getPluginManager().registerEvents(this,plugin);
    }

    public Teams getPlayerTeam(Player player) {
        return teamConnector.containsKey(player) ? teamConnector.get(player) : null;
    }


    public void setPlayerTeam(Player player ,Teams team) {
        if(getPlayerTeam(player) != null)
            getPlayerTeam(player).getTeamPlayers().remove(player);
        team.getTeamPlayers().add(player);


        teamConnector.put(player, team);
        player.setDisplayName(team.getChatColor() + player.getName());
        ScoreUtils.setPacketScore(player);
    }

    public void removePlayerFromTeam(Player player){
        if (teamConnector.containsKey(player)){
            Teams playerTeam = teamConnector.get(player);
            playerTeam.getTeamPlayers().remove(player);
            teamConnector.remove(player);
        }
    }

    public void sortInPLayer(Player player) {
        Teams team = getLeastPlayeraTeam();
        setPlayerTeam(player, team);
        player.sendMessage(BedWars.PREFIX + "§7Du wurdest " + team.getChatColor() + "Team " + team.getTeamName() + "§7 zugeordnet.");
    }

    public void balanceTeams() {
        int offset = getMostPlayersTeam().getTeamPlayers().size() - getLeastPlayeraTeam().getTeamPlayers().size();
        if(offset >= MAY_BALANCE_OFFSET) {
            Bukkit.broadcastMessage(BedWars.PREFIX + "§7Durch Ungleichheiten wurden die Teams neu verteilt!");
            while(offset >= MAY_BALANCE_OFFSET) {
                Player player = getMostPlayersTeam().getTeamPlayers().get(getMostPlayersTeam().getTeamPlayers().size() - 1);
                setPlayerTeam(player, getLeastPlayeraTeam());
                Teams team = getPlayerTeam(player);
                player.sendMessage(BedWars.PREFIX + "§7Du wurdest in " + team.getChatColor() + "Team " + team.getTeamName() + "§7 umsortiert!");
                offset = getMostPlayersTeam().getTeamPlayers().size() - getLeastPlayeraTeam().getTeamPlayers().size();
            }
        }

    }

    private Teams getLeastPlayeraTeam() {
        Teams currentLeastPlayerTeam = Teams.values()[0];
        for(Teams current : plugin.getAllTeams()) {
            if(current.getTeamPlayers().size() < currentLeastPlayerTeam.getTeamPlayers().size())
                currentLeastPlayerTeam = current;
        }
        return currentLeastPlayerTeam;

    }

    private Teams getMostPlayersTeam() {
        Teams currentMostPlayerTeam = Teams.values()[0];
        for(Teams current : plugin.getAllTeams()) {
            if(current.getTeamPlayers().size() > currentMostPlayerTeam.getTeamPlayers().size()) {
                currentMostPlayerTeam = current;

            }

        }
        return currentMostPlayerTeam;
    }

    @EventHandler
    public void onInv(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (plugin.getGameManager().getGameState() == GameState.LOBBY) {
            try {
                if (event.getClickedInventory().getName().equalsIgnoreCase("§6§lTeamAuswahl")) {
                    event.setCancelled(true);

                    for (Teams teams : Teams.values()) {
                        if (event.getCurrentItem().getData().getData() == (byte) teams.getColorID()) {
                            if (getPlayerTeam(player) != null) {
                                if (getPlayerTeam(player) != teams) {
                                    if (teams.getTeamPlayers().size() != GameManager.MAX_PLAYERS_PER_TEAM){
                                        setPlayerTeam(player, teams);
                                        player.closeInventory();
                                        player.sendMessage(BedWars.PREFIX + "§7Du bist nun in Team " + teams.getPREFIX());
                                    } else {
                                        player.closeInventory();
                                        player.sendMessage(BedWars.PREFIX + "§cDieses Team ist bereits voll");
                                    }
                                } else {
                                    player.closeInventory();
                                    player.sendMessage(BedWars.PREFIX + "§cDu bist bereits in diesem Team.");
                                }
                            } else {
                                if (teams.getTeamPlayers().size() != GameManager.MAX_PLAYERS_PER_TEAM){
                                    setPlayerTeam(player, teams);
                                    player.closeInventory();
                                    player.sendMessage(BedWars.PREFIX + "§7Du bist nun in Team " + teams.getPREFIX());
                                } else {
                                    player.closeInventory();
                                    player.sendMessage(BedWars.PREFIX + "§cDieses Team ist bereits voll");
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
            }
        }
    }

    public Inventory getSelectionInventory() {
        Inventory inv = Bukkit.createInventory(null, 9 *1 , "§6§lTeamauswahl");
        for(Teams current : plugin.getAllTeams()) {
            List<String> teamplayers = new ArrayList<>();
            if (current.getTeamPlayers().size() != 0){
                for (Player teamP : current.getTeamPlayers()){
                    teamplayers.add("§8» §7"+current.getChatColor()+teamP.getName());
                }
                inv.addItem(new ItemBuilder(Material.WOOL, (short) current.getColorID()).setDispplayName(current.getChatColor() + current.getTeamName()).addLore(teamplayers).build());
            } else {
                inv.addItem(new ItemBuilder(Material.WOOL, (short) current.getColorID()).setDispplayName(current.getChatColor() + current.getTeamName()).build());
            }
        }
        return inv;
    }

}
