package de.nxtlinx.bedwars.teams;

import de.nxtlinx.bedwars.BedWars;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class TeamIngame {

    private BedWars plugin;

    private ArrayList<Teams> allTeams;
    private ArrayList<Teams> alive;
    private ArrayList<Teams> hasBed;

    public TeamIngame(BedWars plugin){
        this.plugin = plugin;
        this.alive = new ArrayList<>();
        this.hasBed = new ArrayList<>();
        this.allTeams = new ArrayList<>();

    }

    public void addAllTeams(){
        TeamManager teamManager = plugin.getTeamManager();
        for (Player all : plugin.getGameManager().getPlayers()){
            Teams teams = teamManager.getPlayerTeam(all);
            if (teams != null){
                if (!getHasBed().contains(teams)){
                    addAll(teams);
                }
            }
        }
    }

    public void addSpectator(Player player){
        if (plugin.getGameManager().getPlayers().contains(player)){
            plugin.getGameManager().getPlayers().remove(player);
        }
        if (!plugin.getGameManager().getSpectators().contains(player)){
            plugin.getGameManager().getSpectators().add(player);
        }
    }

    public void removeBed(Teams teams){
        if (hasBed.contains(teams)){
            hasBed.remove(teams);
            for (Player all : Bukkit.getOnlinePlayers()){
                if (all.getName().equals("NxtLinx")){
                    all.sendMessage(teams.getPREFIX()+" §chas been removed");
                }
            }
        }
    }

    public void removeAlive(Teams teams){
            alive.remove(teams);
    }

    public void removeTeamAll(Teams teams){
        if (allTeams.contains(teams)){
            allTeams.remove(teams);
        }
        if (hasBed.contains(teams)){
            hasBed.remove(teams);
        }
        Bukkit.getConsoleSender().sendMessage("Team remove: "+teams.getPREFIX());
    }

    private void addAll(Teams teams){
        //  allTeams.add(teams);
        //    alive.add(teams);
            hasBed.add(teams);
    }

    public ArrayList<Teams> getAlive() {
        return alive;
    }

    public ArrayList<Teams> getHasBed() {
        return hasBed;
    }
}
