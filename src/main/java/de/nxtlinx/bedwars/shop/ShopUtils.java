package de.nxtlinx.bedwars.shop;


import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

public class ShopUtils {

    private int prize;
    private Material prizeType;
    private Material give;
    private int giveAmount;

    private int place;

    private String shopName;
    private Kategorie kategorie;

    private Enchantment enchantment;
    private int level;


  //  public ShopUtils(){ }

    public ShopUtils create(Kategorie kategorie,Material prizeType,int prize,Material give,int giveAmount,String shopName){
        this.kategorie = kategorie;
        this.prizeType = prizeType;
        this.prize = prize;
        this.give = give;
        this.giveAmount = giveAmount;
        this.shopName = shopName;
        return this;
    }

    public ShopUtils create(Kategorie kategorie,Material prizeType,int prize,Material give,int giveAmount,String shopName,int place){
        this.kategorie = kategorie;
        this.prizeType = prizeType;
        this.prize = prize;
        this.give = give;
        this.giveAmount = giveAmount;
        this.shopName = shopName;
        this.place = place;
        return  this;
    }

    public ShopUtils addEnchant(Enchantment enchantment,int level){
        this.enchantment = enchantment;
        this.level = level;
        return this;
    }

    public Enchantment getEnchantment() {
        return enchantment;
    }

    public int getLevel() {
        return level;
    }

    public int getPrize() {
        return prize;
    }

    public Material getPrizeType() {
        return prizeType;
    }

    public Material getGive() {
        return give;
    }

    public int getGiveAmount() {
        return giveAmount;
    }

    public String getShopName() {
        return shopName;
    }

    public Kategorie getKategorie() {
        return kategorie;
    }

    public int getPlace() {
        return place;
    }
}
