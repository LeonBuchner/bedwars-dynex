package de.nxtlinx.bedwars.shop;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

public enum Kategorie {

    SHORT("§7Schnellzugriff", Bukkit.createInventory(null,9*4,"§7Schnellzugriff"),-1,Material.STICK),  // 1
    BLOCKS("§7Blöcke", Bukkit.createInventory(null,9*4,"§7Blöcke"),1,Material.SANDSTONE),  // 5
    AMOUR("§7Rüstungen", Bukkit.createInventory(null,9*4,"§7Rüstungen"),2,Material.CHAINMAIL_CHESTPLATE), // 7
    TOOLS("§7Spitzhacken", Bukkit.createInventory(null,9*4,"§7Spitzhacken"),3,Material.WOOD_PICKAXE),  // 3
    WEAPONS("§7Waffen", Bukkit.createInventory(null,9*4,"§7Waffen"),4,Material.WOOD_SWORD), // 2
    BOW("§7Bogen", Bukkit.createInventory(null,9*4,"§7Bogen"),5,Material.BOW), // 4
    EATING("§7Essen",Bukkit.createInventory(null,9*4,"§7Essen"),6,Material.COOKED_BEEF), // 8
    SPEZIALS("§7Spezial", Bukkit.createInventory(null,9*4,"§7Spezial"),7,Material.POTION); // 6



    private String shopName;
    private Inventory inventory;
    private int size;
    private Material icon;

    private Kategorie(String shopName,Inventory inventory,int size,Material icon){
        this.shopName = shopName;
        this.inventory = inventory;
        this.size = size;
        this.icon = icon;
    }

    public String getShopName() {
        return shopName;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public int getSize() {
        return size;
    }

    public Material getIcon() {
        return icon;
    }
}
