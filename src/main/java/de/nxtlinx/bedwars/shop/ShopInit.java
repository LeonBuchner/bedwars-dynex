package de.nxtlinx.bedwars.shop;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.utils.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;

public class ShopInit {

    private BedWars plugin;
    private ArrayList<ShopUtils> items;

    public ShopInit(BedWars plugin){
        this.plugin = plugin;
        items = new ArrayList<ShopUtils>();
        addItemsToShop();
    }

    private void addItemsToShop(){
        items.add(new ShopUtils().create(Kategorie.SHORT, Material.CLAY_BRICK,1,Material.SANDSTONE,2,"§3Sandstein"));
        items.add(new ShopUtils().create(Kategorie.SHORT, Material.CLAY_BRICK,5,Material.STICK,1,"§3Knüppel").addEnchant(Enchantment.KNOCKBACK,1));
        items.add(new ShopUtils().create(Kategorie.SHORT, Material.CLAY_BRICK,7,Material.WOOD_PICKAXE,1,"§3Spitzhacke I"));
        items.add(new ShopUtils().create(Kategorie.SHORT, Material.IRON_INGOT,1,Material.CHAINMAIL_CHESTPLATE,1,"§3Brustplatte"));

        items.add(new ShopUtils().create(Kategorie.WEAPONS, Material.CLAY_BRICK,5,Material.STICK,1,"§3Knüppel").addEnchant(Enchantment.KNOCKBACK,1));
        items.add(new ShopUtils().create(Kategorie.WEAPONS, Material.IRON_INGOT,1,Material.WOOD_SWORD,1,"§3Holz Schwert"));
        items.add(new ShopUtils().create(Kategorie.WEAPONS, Material.IRON_INGOT,4,Material.WOOD_SWORD,1,"§3Holz Schwert I").addEnchant(Enchantment.DAMAGE_ALL,1));
        items.add(new ShopUtils().create(Kategorie.WEAPONS, Material.IRON_INGOT,6,Material.WOOD_SWORD,1,"§3Holz Schwert II").addEnchant(Enchantment.DAMAGE_ALL,2));
        items.add(new ShopUtils().create(Kategorie.WEAPONS, Material.GOLD_INGOT,6,Material.IRON_SWORD,1,"§3Eisen Schwert").addEnchant(Enchantment.DAMAGE_ALL,2).addEnchant(Enchantment.KNOCKBACK,1));
        items.add(new ShopUtils().create(Kategorie.WEAPONS, Material.GOLD_INGOT,5,Material.WOOD_SWORD,1,"§3Spieler Tracker"));

        items.add(new ShopUtils().create(Kategorie.BLOCKS, Material.CLAY_BRICK,1,Material.SANDSTONE,2,"§3Sandstein"));
        items.add(new ShopUtils().create(Kategorie.BLOCKS, Material.CLAY_BRICK,2,Material.SANDSTONE_STAIRS,1,"§3Sandstein Treppen"));
        items.add(new ShopUtils().create(Kategorie.BLOCKS, Material.CLAY_BRICK,5,Material.ENDER_STONE,1,"§3Endstein"));
        items.add(new ShopUtils().create(Kategorie.BLOCKS, Material.IRON_INGOT,3,Material.IRON_BLOCK,1,"§3Eisenblock"));
        items.add(new ShopUtils().create(Kategorie.BLOCKS, Material.CLAY_BRICK,2,Material.GLASS,1,"§3Glas"));
        items.add(new ShopUtils().create(Kategorie.BLOCKS, Material.CLAY_BRICK,14,Material.GLOWSTONE,4,"§3Glowstone"));
        items.add(new ShopUtils().create(Kategorie.BLOCKS, Material.CLAY_BRICK,15,Material.SOUL_SAND,1,"§3Seelensand"));

        items.add(new ShopUtils().create(Kategorie.AMOUR, Material.CLAY_BRICK,1,Material.LEATHER_HELMET,1,"§3Lederhelm"));
        items.add(new ShopUtils().create(Kategorie.AMOUR, Material.CLAY_BRICK,1,Material.LEATHER_CHESTPLATE,1,"§3Lederbrustplatte"));
        items.add(new ShopUtils().create(Kategorie.AMOUR, Material.CLAY_BRICK,1,Material.LEATHER_LEGGINGS,1,"§3Lederhose"));
        items.add(new ShopUtils().create(Kategorie.AMOUR, Material.CLAY_BRICK,1,Material.LEATHER_BOOTS,1,"§3Lederschuhe"));
        items.add(new ShopUtils().create(Kategorie.AMOUR, Material.IRON_INGOT,1,Material.CHAINMAIL_CHESTPLATE,1,"§3Brustplatte"));
        items.add(new ShopUtils().create(Kategorie.AMOUR, Material.IRON_INGOT,3,Material.CHAINMAIL_CHESTPLATE,1,"§3Brustplatte I").addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL,1));
        items.add(new ShopUtils().create(Kategorie.AMOUR, Material.IRON_INGOT,7,Material.CHAINMAIL_CHESTPLATE,1,"§3Brustplatte II").addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL,2));
        items.add(new ShopUtils().create(Kategorie.AMOUR, Material.IRON_INGOT,11,Material.CHAINMAIL_CHESTPLATE,1,"§3Brustplatte III").addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL,1).addEnchant(Enchantment.THORNS,1));

        items.add(new ShopUtils().create(Kategorie.TOOLS, Material.CLAY_BRICK,7,Material.WOOD_PICKAXE,1,"§3Spitzhacke I"));
        items.add(new ShopUtils().create(Kategorie.TOOLS, Material.IRON_INGOT,2,Material.STONE_PICKAXE,1,"§3Spitzhacke II"));
        items.add(new ShopUtils().create(Kategorie.TOOLS, Material.GOLD_INGOT,1,Material.IRON_PICKAXE,1,"§3Spitzhacke III"));

        items.add(new ShopUtils().create(Kategorie.BOW, Material.GOLD_INGOT,3,Material.BOW,1,"§3Bogen I").addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL,1).addEnchant(Enchantment.ARROW_INFINITE,1));
        items.add(new ShopUtils().create(Kategorie.BOW, Material.GOLD_INGOT,7,Material.BOW,1,"§3Bogen II").addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL,1).addEnchant(Enchantment.ARROW_INFINITE,1).addEnchant(Enchantment.ARROW_DAMAGE,1));
        items.add(new ShopUtils().create(Kategorie.BOW, Material.GOLD_INGOT,11,Material.BOW,1,"§3Bogen III").addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL,1).addEnchant(Enchantment.ARROW_INFINITE,1).addEnchant(Enchantment.ARROW_DAMAGE,2));
        items.add(new ShopUtils().create(Kategorie.BOW, Material.GOLD_INGOT,1,Material.ARROW,1,"§3Pfeil"));

        items.add(new ShopUtils().create(Kategorie.SPEZIALS, Material.CLAY_BRICK,15,Material.WEB,1,"§3Spinnennetz"));
        items.add(new ShopUtils().create(Kategorie.SPEZIALS, Material.GOLD_INGOT,11,Material.ENDER_PEARL,1,"§3Enderperle"));
       // items.add(new ShopUtils().create(Kategorie.SPEZIALS, Material.CLAY_BRICK,11,Material.BLAZE_ROD,1,"§3Rettungsplattform"));
        items.add(new ShopUtils().create(Kategorie.SPEZIALS,Material.CLAY_BRICK,3,Material.CARPET,1,"§7SpeedTeppich"));

        items.add(new ShopUtils().create(Kategorie.EATING,Material.CLAY_BRICK,1,Material.APPLE,1,"§7Apfel"));
        items.add(new ShopUtils().create(Kategorie.EATING,Material.CLAY_BRICK,3,Material.COOKED_BEEF,1,"§7Steak"));
        items.add(new ShopUtils().create(Kategorie.EATING,Material.IRON_INGOT,1,Material.CAKE,1,"§7Kuchen"));
        items.add(new ShopUtils().create(Kategorie.EATING,Material.GOLD_INGOT,5,Material.GOLDEN_APPLE,2,"§7Goldener-Apfel"));
    }

    public Inventory openKategorie(Kategorie kategorie){
        Inventory inventory = kategorie.getInventory();
        for (int i = 0; i < inventory.getSize() ; i++) {
            inventory.setItem(i,new ItemBuilder(Material.STAINED_GLASS_PANE).setNoName().build());
        }
        for (Kategorie kategorien : Kategorie.values()){
            if (kategorien != Kategorie.SHORT){
                inventory.setItem(kategorien.getSize(),new ItemBuilder(kategorien.getIcon()).setDispplayName(kategorien.getShopName()).build());
            }
        }
        int platz = 19;
        for (ShopUtils shopUtils : items){
            if (shopUtils.getKategorie() == kategorie){
                if (shopUtils.getPrizeType() == Material.CLAY_BRICK){
                    inventory.setItem(platz,new ItemBuilder(shopUtils.getGive()).setDispplayName(shopUtils.getShopName()).addLore("§6Preis: §7"+shopUtils.getPrize()+" " +
                            "§6Material: §7Bronze").build());
                    platz++;
                }
                if (shopUtils.getPrizeType() == Material.IRON_INGOT){
                    inventory.setItem(platz,new ItemBuilder(shopUtils.getGive()).setDispplayName(shopUtils.getShopName()).addLore("§6Preis: §7"+shopUtils.getPrize()+" " +
                            "§6Material: §7Eisen").build());
                    platz++;
                }
                if (shopUtils.getPrizeType() == Material.GOLD_INGOT){
                    inventory.setItem(platz,new ItemBuilder(shopUtils.getGive()).setDispplayName(shopUtils.getShopName()).addLore("§6Preis: §7"+shopUtils.getPrize()+" " +
                            "§6Material: §7Gold").build());
                    platz++;
                }
            }
        }
        return inventory;
    }

    public Inventory openKategorie(String shopname){
        Inventory inventory = null;
        Kategorie kategorie = null;
        for (Kategorie kategorien : Kategorie.values()){
            if (kategorien.getShopName().equals(shopname)){
             inventory = kategorien.getInventory();
             kategorie = kategorien;
            }
        }
        for (int i = 0; i < inventory.getSize() ; i++) {
            inventory.setItem(i,new ItemBuilder(Material.STAINED_GLASS_PANE).setNoName().build());
        }
        for (Kategorie setItems : Kategorie.values()){
            if (setItems != Kategorie.SHORT){
                inventory.setItem(setItems.getSize(),new ItemBuilder(setItems.getIcon()).setDispplayName(setItems.getShopName()).build());
            }
        }
        int platz = 19;
        for (ShopUtils shopUtils : items){
            if (shopUtils.getKategorie() == kategorie){
                if (shopUtils.getPrizeType() == Material.CLAY_BRICK){
                    inventory.setItem(platz,new ItemBuilder(shopUtils.getGive()).setDispplayName(shopUtils.getShopName()).addLore("§6Preis: §7"+shopUtils.getPrize()+" " +
                            "§6Material: §7Bronze").build());
                    platz++;
                }
                if (shopUtils.getPrizeType() == Material.IRON_INGOT){
                    inventory.setItem(platz,new ItemBuilder(shopUtils.getGive()).setDispplayName(shopUtils.getShopName()).addLore("§6Preis: §7"+shopUtils.getPrize()+" " +
                            "§6Material: §7Eisen").build());
                    platz++;
                }
                if (shopUtils.getPrizeType() == Material.GOLD_INGOT){
                    inventory.setItem(platz,new ItemBuilder(shopUtils.getGive()).setDispplayName(shopUtils.getShopName()).addLore("§6Preis: §7"+shopUtils.getPrize()+" " +
                            "§6Material: §7Gold").build());
                    platz++;
                }
            }
        }
        return inventory;
    }

    public ArrayList<ShopUtils> getItems() {
        return items;
    }
}
