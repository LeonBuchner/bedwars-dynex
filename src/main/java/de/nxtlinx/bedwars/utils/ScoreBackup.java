package de.nxtlinx.bedwars.utils;

import de.nxtlinx.DynexAPI;
import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.gamemanager.GameState;
import de.nxtlinx.bedwars.teams.TeamManager;
import de.nxtlinx.bedwars.teams.Teams;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.HashMap;

public class ScoreBackup {

    private BedWars plugin;
    private HashMap<Teams,String> enumHash;
    private int specSize;
    private int playerSize;

    public ScoreBackup(BedWars plugin){
        this.plugin = plugin;
        enumHash = new HashMap<>();
        teamInit();
    }

    private static ScoreboardObjective scoreboardObjective = BedWars.getPlugin().getScoreboardScore().registerObjective("§3Bed§bWars", IScoreboardCriteria.b);

    public static void setPacketScore(Player player){

        if (BedWars.getPlugin().getGameManager().getGameState() == GameState.LOBBY) {
            net.minecraft.server.v1_8_R3.Scoreboard scoreboard = BedWars.getPlugin().getScoreboardScore();

            PacketPlayOutScoreboardObjective create = new PacketPlayOutScoreboardObjective(scoreboardObjective, 0);
            PacketPlayOutScoreboardObjective remove = new PacketPlayOutScoreboardObjective(scoreboardObjective, 1);
            PacketPlayOutScoreboardDisplayObjective displayObjective = new PacketPlayOutScoreboardDisplayObjective(1, scoreboardObjective);
            scoreboardObjective.setDisplayName("§8» §3Bed§bWars §8«");

            ScoreboardScore scoreboardScore012 = null;
            ScoreboardScore scoreboardScore011 = null;
            ScoreboardScore scoreboardScore010 = null;
            ScoreboardScore scoreboardScore009 = null;
            ScoreboardScore scoreboardScore008 = null;
            ScoreboardScore scoreboardScore007 = null;
            ScoreboardScore scoreboardScore006 = null;
            ScoreboardScore scoreboardScore005 = null;
            ScoreboardScore scoreboardScore004 = null;

            scoreboardScore012 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7§9");
            scoreboardScore011 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Map §8» §6"+BedWars.MAP);
            scoreboardScore010 = new ScoreboardScore(scoreboard, scoreboardObjective, "§9§6");
            TeamManager teamManager = BedWars.getPlugin().getTeamManager();
            Teams teams = teamManager.getPlayerTeam(player);
            if (teams != null){
                scoreboardScore009 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Team §8» §6"+teams.getPREFIX());
            } else {
                scoreboardScore009 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Team §8» §6Zufall");
            }
            scoreboardScore008 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7§4§2");
            if (BedWars.getPlugin().isDynexapi()){
                if (DynexAPI.getPlugin().getMySQL().isConnected()){
                    scoreboardScore007 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Coins §8» §6"+DynexAPI.getPlugin().getCoinsAPI().getCoins(player.getName()));
                } else {
                    scoreboardScore007 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Coins §8» §6keine");
                }
            } else {
                scoreboardScore007 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Coins §8» §6keine");
            }
            scoreboardScore006 = new ScoreboardScore(scoreboard, scoreboardObjective, "§5§8§9");
            if (BedWars.getPlugin().isDynexapi()){
                if (DynexAPI.getPlugin().getMySQL().isConnected()){
                    scoreboardScore005 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Rang §8» §6"+DynexAPI.getPlugin().getTeamName(player));
                } else {
                    scoreboardScore005 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Rang §8» §6keine");
                }
            } else {
                scoreboardScore005 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Rang §8» §6keine");
            }
            scoreboardScore004 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7§6§4");




            scoreboardScore012.setScore(9);
            scoreboardScore011.setScore(8);
            scoreboardScore010.setScore(7);
            scoreboardScore009.setScore(6);
            scoreboardScore008.setScore(5);
            scoreboardScore007.setScore(4);
            scoreboardScore006.setScore(3);
            scoreboardScore005.setScore(2);
            scoreboardScore004.setScore(1);


            PacketPlayOutScoreboardScore registerScore12 = new PacketPlayOutScoreboardScore(scoreboardScore012);
            PacketPlayOutScoreboardScore registerScore11 = new PacketPlayOutScoreboardScore(scoreboardScore011);
            PacketPlayOutScoreboardScore registerScore10 = new PacketPlayOutScoreboardScore(scoreboardScore010);
            PacketPlayOutScoreboardScore registerScore09 = new PacketPlayOutScoreboardScore(scoreboardScore009);
            PacketPlayOutScoreboardScore registerScore08 = new PacketPlayOutScoreboardScore(scoreboardScore008);
            PacketPlayOutScoreboardScore registerScore07 = new PacketPlayOutScoreboardScore(scoreboardScore007);
            PacketPlayOutScoreboardScore registerScore06 = new PacketPlayOutScoreboardScore(scoreboardScore006);
            PacketPlayOutScoreboardScore registerScore05 = new PacketPlayOutScoreboardScore(scoreboardScore005);
            PacketPlayOutScoreboardScore registerScore04 = new PacketPlayOutScoreboardScore(scoreboardScore004);

            sendPacket(player, remove);
            sendPacket(player, create);
            sendPacket(player, displayObjective);
            sendPacket(player, registerScore12);
            sendPacket(player,registerScore11);
            sendPacket(player,registerScore10);
            sendPacket(player,registerScore09);
            sendPacket(player,registerScore08);
            sendPacket(player,registerScore07);
            sendPacket(player,registerScore06);
            sendPacket(player,registerScore05);
            sendPacket(player,registerScore04);


        }



        if (BedWars.getPlugin().getGameManager().getGameState() == GameState.INGAME) {
            net.minecraft.server.v1_8_R3.Scoreboard scoreboard = BedWars.getPlugin().getScoreboardScore();

            PacketPlayOutScoreboardObjective create = new PacketPlayOutScoreboardObjective(scoreboardObjective, 0);
            PacketPlayOutScoreboardObjective remove = new PacketPlayOutScoreboardObjective(scoreboardObjective, 1);
            PacketPlayOutScoreboardDisplayObjective displayObjective = new PacketPlayOutScoreboardDisplayObjective(1, scoreboardObjective);
            scoreboardObjective.setDisplayName("§8» §3Bed§bWars §8«");


            int place = 1;
            final ArrayList<ScoreUtilsObject> scoreArray = new ArrayList<>();
            //  for (Teams teams : Teams.values()) {
            for (Teams teams : BedWars.getPlugin().getAllTeams()) {
                if (BedWars.getPlugin().getGameManager().getTeamIngameManager().getHasBed().contains(teams)){
                    scoreArray.add(new ScoreUtilsObject().create(teams,new ScoreboardScore(scoreboard,scoreboardObjective,teams.getPREFIX()+" §8» §a✔"),place));
                    place++;
                } else {
                    scoreArray.add(new ScoreUtilsObject().create(teams,new ScoreboardScore(scoreboard,scoreboardObjective,teams.getPREFIX()+" §8» §4✖"),place));
                    place++;
                }
            }

            ScoreboardScore scoreboardScore012 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7Teams§8:");
            scoreboardScore012.setScore(12);
            ScoreboardScore scoreboardScore011 = new ScoreboardScore(scoreboard, scoreboardObjective, "§7§8§3");
            scoreboardScore011.setScore(11);

            final ArrayList<PacketPlayOutScoreboardScore> packetHash = new ArrayList<>();

            for (int i = 0; i < scoreArray.size() ; i++) {
                scoreArray.get(i).setScore();
                packetHash.add(new PacketPlayOutScoreboardScore(scoreArray.get(i).getScoreboardScore()));
            }
            PacketPlayOutScoreboardScore registerScore12 = new PacketPlayOutScoreboardScore(scoreboardScore012);
            PacketPlayOutScoreboardScore registerScore11 = new PacketPlayOutScoreboardScore(scoreboardScore011);

            sendPacket(player, remove);
            sendPacket(player, create);
            sendPacket(player, displayObjective);
            sendPacket(player, registerScore12);
            sendPacket(player,registerScore11);

            for (int i = 0; i < packetHash.size() ; i++) {
                sendPacket(player,packetHash.get(i));
            }
        }

    }


    private static void sendPacket(Player player, Packet<?> packet) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

    private void teamInit(){
        int size = 1;
        //  for (Teams teamsEnum : Teams.values()){
        for (Teams teamsEnum : BedWars.getPlugin().getAllTeams()) {
            String name = "00"+size+teamsEnum.getTeamName();
            Team team = plugin.getScoreboard().registerNewTeam(name);
            team.setAllowFriendlyFire(true);
            team.setPrefix(teamsEnum.getChatColor()+"⬛ §7» §7");
            enumHash.put(teamsEnum,name);
            size++;
        }
        specSize = size;
        String spec = "00"+size+"Spec";
        Team specTeam = plugin.getScoreboard().registerNewTeam(spec);
        specTeam.setAllowFriendlyFire(true);
        specTeam.setPrefix("§7SPEC §7» §7");
        size++;
        playerSize = size;
        String player = "00"+size+"player";
        Team firstJoin = plugin.getScoreboard().registerNewTeam(player);
        firstJoin.setAllowFriendlyFire(false);
        firstJoin.setPrefix("§7Spieler §7» §7");
    }

    public void addTeam(Player player,Teams teams){
        if (enumHash.containsKey(teams)){
            org.bukkit.scoreboard.Scoreboard scoreboard = plugin.getScoreboard();
            Team team = scoreboard.getTeam(enumHash.get(teams));
            if (plugin.getLabyModListener().getLabyModUsers().contains(player)){
                team.setSuffix(" §3Ⓛ");
            }
            team.addEntry(player.getName());
            for (Player all : Bukkit.getOnlinePlayers()){
                all.setScoreboard(scoreboard);
            }
            //  team.setSuffix("");
        }
    }

    public void addTeamSpectator(Player player){
        org.bukkit.scoreboard.Scoreboard scoreboard = plugin.getScoreboard();
        Team team = scoreboard.getTeam("00"+specSize+"Spec");
        team.addEntry(player.getName());
        for (Player all : Bukkit.getOnlinePlayers()){
            all.setScoreboard(scoreboard);
        }
    }

    public void addPlayer(Player player){
        org.bukkit.scoreboard.Scoreboard scoreboard = plugin.getScoreboard();
        Team team = scoreboard.getTeam("00"+playerSize+"player");
        if (plugin.getLabyModListener().getLabyModUsers().contains(player)){
            team.setSuffix(" §3Ⓛ");
        }
        team.addEntry(player.getName());
        for (Player all : Bukkit.getOnlinePlayers()){
            all.setScoreboard(scoreboard);
        }
    }

    public void removeFromTeam(Player player){
        org.bukkit.scoreboard.Scoreboard scoreboard = plugin.getScoreboard();
        Team team = scoreboard.getEntryTeam(player.getName());
        team.removeEntry(player.getName());
        for (Player all : Bukkit.getOnlinePlayers()){
            all.setScoreboard(scoreboard);
        }
    }

}
