package de.nxtlinx.bedwars.utils;

import de.nxtlinx.bedwars.teams.Teams;
import net.minecraft.server.v1_8_R3.ScoreboardScore;
import org.bukkit.entity.Player;

public class ScoreUtilsObject {

    private Teams teams;
    private ScoreboardScore scoreboardScore;
    private int place;

    public ScoreUtilsObject(){ }

    public ScoreUtilsObject create(Teams teams,ScoreboardScore scoreboardScore,int place){
        this.teams = teams;
        this.scoreboardScore = scoreboardScore;
        this.place = place;
        return this;
    }

    public void setScore(){
        this.getScoreboardScore().setScore(this.place);
    }

    public Teams getTeams() {
        return teams;
    }

    public ScoreboardScore getScoreboardScore() {
        return scoreboardScore;
    }

    public int getPlace() {
        return place;
    }
}
