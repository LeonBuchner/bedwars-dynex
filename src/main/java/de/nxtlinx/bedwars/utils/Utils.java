package de.nxtlinx.bedwars.utils;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class Utils {

    public static void setJoinItems(Player player){
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setGameMode(GameMode.ADVENTURE);
        player.getInventory().setItem(8,new ItemBuilder(Material.CLAY_BALL).setDispplayName("§cSpiel verlassen").build());
        player.getInventory().setItem(0,new ItemBuilder(Material.COMPASS).setDispplayName("§6Team-Auswahl").build());
     //   player.getInventory().setItem(2,new ItemBuilder(Material.PAPER).setDispplayName("§7Map-Voting").build());
        player.getInventory().setItem(4,new ItemBuilder(Material.GOLD_INGOT).setDispplayName("§7Gold-Voting").build());
        player.updateInventory();
    }

    public static void setEndingItems(Player player){
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setGameMode(GameMode.ADVENTURE);
        player.getInventory().setItem(8,new ItemBuilder(Material.CLAY_BALL).setDispplayName("§cSpiel verlassen").build());
        player.updateInventory();
    }

    public static void setSpectator(Player player){
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setGameMode(GameMode.ADVENTURE);
        player.getInventory().setItem(8,new ItemBuilder(Material.CLAY_BALL).setDispplayName("§cSpiel verlassen").build());
        player.getInventory().setItem(1,new ItemBuilder(Material.COMPASS).setDispplayName("§6Teleporter").build());
        player.updateInventory();
    }

}
