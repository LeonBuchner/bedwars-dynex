package de.nxtlinx.bedwars.utils;

import de.nxtlinx.bedwars.BedWars;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

public class ShopRemove {

    private HashMap<Player, ArrayList<Integer>> addMaterialArray;
    private HashMap<Player, Integer> materials;

    public ShopRemove(){
        addMaterialArray = new HashMap<>();
        materials = new HashMap<>();
    }

    public boolean check(Player player, Material removeItem, int preis) throws Exception {
        invCheck(player, removeItem);
        set(player,removeItem,materials.get(player));
        ArrayList<Integer> moreinv = new ArrayList<>();
        for (int i = 0; i < player.getInventory().getSize(); i++) {
            if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType() == removeItem) {
                moreinv.add(i);
            }
        }
        addMaterialArray.put(player, moreinv);
        if (addMaterialArray.get(player).size() == 0) {
            return false;
        } else if (addMaterialArray.get(player).size() == 1) {
            if (player.getInventory().getItem(addMaterialArray.get(player).get(0)).getAmount() >= preis) {
                if (player.getInventory().getItem(addMaterialArray.get(player).get(0)).getAmount() == preis) {
                    player.getInventory().remove(removeItem);
                    player.updateInventory();
                    remove(player);
                    return true;
                } else {
                    player.getInventory().getItem(addMaterialArray.get(player).get(0)).setAmount(player.getInventory().getItem(addMaterialArray.get(player).get(0)).getAmount() - preis);
                    return true;
                }
            } else {
                if (removeItem == Material.CLAY_BRICK){
                    player.sendMessage(BedWars.PREFIX+"§cDu hast zu wenig §6Bronze");
                }
                if (removeItem == Material.IRON_INGOT){
                    player.sendMessage(BedWars.PREFIX+"§cDu hast zu wenig §6Eisen");
                }
                if (removeItem == Material.GOLD_INGOT){
                    player.sendMessage(BedWars.PREFIX+"§cDu hast zu wenig §6Gold");
                }
                return false;
            }
        } else {
            for (int i = 0; i < addMaterialArray.get(player).size(); i++) {
                if (player.getInventory().getItem(addMaterialArray.get(player).get(i)).getAmount() >= preis) {
                    if (player.getInventory().getItem(addMaterialArray.get(player).get(i)).getAmount() == preis) {
                        player.getInventory().remove(removeItem);
                        player.updateInventory();
                        remove(player);
                        return true;
                    } else {
                        player.getInventory().getItem(addMaterialArray.get(player).get(i)).setAmount(player.getInventory().getItem(addMaterialArray.get(player).get(i)).getAmount() - preis);
                        return true;
                    }
                } else {
                    if (removeItem == Material.CLAY_BRICK){
                        player.sendMessage(BedWars.PREFIX+"§cDu hast zu wenig §6Bronze");
                    }
                    if (removeItem == Material.IRON_INGOT){
                        player.sendMessage(BedWars.PREFIX+"§cDu hast zu wenig §6Eisen");
                    }
                    if (removeItem == Material.GOLD_INGOT){
                        player.sendMessage(BedWars.PREFIX+"§cDu hast zu wenig §6Gold");
                    }
                    return false;
                }
            }
        }
        return false;
    }

    public boolean checkShift(Player player, Material removeItem, int preis) throws Exception {
        invCheck(player, removeItem);
        set(player,removeItem,materials.get(player));
        ArrayList<Integer> moreinv = new ArrayList<>();
        for (int i = 0; i < player.getInventory().getSize(); i++) {
            if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType() == removeItem) {
                moreinv.add(i);
            }
        }
        addMaterialArray.put(player, moreinv);
        if (addMaterialArray.get(player).size() == 0) {
            return false;
        } else if (addMaterialArray.get(player).size() == 1) {
            if (player.getInventory().getItem(addMaterialArray.get(player).get(0)).getAmount() >= preis) {
                if (player.getInventory().getItem(addMaterialArray.get(player).get(0)).getAmount() == preis) {
                    player.getInventory().remove(removeItem);
                    player.updateInventory();
                    remove(player);
                    return true;
                } else {
                    player.getInventory().getItem(addMaterialArray.get(player).get(0)).setAmount(player.getInventory().getItem(addMaterialArray.get(player).get(0)).getAmount() - preis);
                    return true;
                }
            } else {
                return false;
            }
        } else {
            for (int i = 0; i < addMaterialArray.get(player).size(); i++) {
                if (player.getInventory().getItem(addMaterialArray.get(player).get(i)).getAmount() >= preis) {
                    if (player.getInventory().getItem(addMaterialArray.get(player).get(i)).getAmount() == preis) {
                        player.getInventory().remove(removeItem);
                        player.updateInventory();
                        remove(player);
                        return true;
                    } else {
                        player.getInventory().getItem(addMaterialArray.get(player).get(i)).setAmount(player.getInventory().getItem(addMaterialArray.get(player).get(i)).getAmount() - preis);
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private void mixen (Player player,int amount){
        if (materials.containsKey(player)) {
            materials.put(player, materials.get(player) + amount);
        } else {
            materials.put(player,amount);
        }
    }
    private void remove (Player player){
        if (materials.containsKey(player)) {
            materials.remove(player);
        }
    }

    private void set(Player player, Material material,int amount){
        if (materials.containsKey(player)){
            materials.remove(player);
            player.getInventory().remove(material);
            if (material == Material.CLAY_BRICK){
                player.getInventory().addItem(new ItemBuilder(material).setAmount(amount).setDispplayName("§7Bronze").build());
            }
            if (material == Material.IRON_INGOT){
                player.getInventory().addItem(new ItemBuilder(material).setAmount(amount).setDispplayName("§7Eisen").build());
            }
            if (material == Material.GOLD_INGOT){
                player.getInventory().addItem(new ItemBuilder(material).setAmount(amount).setDispplayName("§7Gold").build());
            }
        }
    }

    private void invCheck (Player player, Material material){
        for (int i = 0; i < player.getInventory().getSize(); i++) {
            if (player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getType() == material) {
                mixen(player, player.getInventory().getItem(i).getAmount());
            }
        }
      //  remove(player);
    }
}
