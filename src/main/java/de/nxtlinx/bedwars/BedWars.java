package de.nxtlinx.bedwars;

import de.nxtlinx.bedwars.commands.*;
import de.nxtlinx.bedwars.countdown.CountdownManager;
import de.nxtlinx.bedwars.gamemanager.GameManager;
import de.nxtlinx.bedwars.gamemanager.VoteManager;
import de.nxtlinx.bedwars.listeners.*;
import de.nxtlinx.bedwars.mysql.FileManager;
import de.nxtlinx.bedwars.mysql.MySQL;
import de.nxtlinx.bedwars.mysql.Top5Wall;
import de.nxtlinx.bedwars.setup.BedUtil;
import de.nxtlinx.bedwars.setup.ItemManager;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.shop.ShopInit;
import de.nxtlinx.bedwars.teams.TeamManager;
import de.nxtlinx.bedwars.teams.Teams;
import de.nxtlinx.bedwars.utils.ScoreUtils;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;

public class BedWars extends JavaPlugin {

    private static BedWars plugin;

    public static final String PREFIX = "§3BedWars §8» §r";
    public static final String ERROR = PREFIX+"§cEs gab einen Fehler im Setup, bitte melde diesen schnellstmöglich dem Team";
    public static String MAP = "Error";

    private GameManager gameManager;
    private TeamManager teamManager;
    private ItemManager itemManager;
    private BedUtil bedUtil;
    private CountdownManager countdownManager;
    private ShopInit shopInit;
    private ArrayList<Teams> allTeams;
    private GameProtection gameProtection;
    private Scoreboard scoreboard;
    private net.minecraft.server.v1_8_R3.Scoreboard scoreboardScore;
    private ScoreUtils scoreUtils;
    private LabyModListeners labyModListener;
    private VoteManager voteManager;
    private MySQL mySQL;
    private Top5Wall top5Wall;

    private boolean forceMap;
    private boolean cloudNet;
    private boolean dynexapi;
    private boolean replay;
    private boolean labymod;
    private boolean gold;
    private boolean goldVote;
    private boolean mapVote;

    @Override
    public void onEnable() {
        plugin = this;
        goldVote = true;
        gold = true;
        mapVote = true;
        cloudNet = false;
        replay = false;
        dynexapi = false;
        forceMap = true;
        labymod = false;
        allTeams = new ArrayList<>();
      //  allTeams.addAll(Arrays.asList(Teams.values()));
        allTeams.add(Teams.BLUE);
        allTeams.add(Teams.RED);
        teamManager = new TeamManager(this);
        gameManager = new GameManager(this);
        bedUtil = new BedUtil(this);
        countdownManager = new CountdownManager(this);
        shopInit = new ShopInit(this);
        new BuildCMD(this);
        gameProtection = new GameProtection(this);
        gameProtection.replace();
        new PlayerConnection(this);
        new ShopListener(this);
        new PlayerDeath(this);
        new PlayerInteract(this);
        new StartCMD(this);
        new ForceMapCMD(this);
        itemManager = new ItemManager(this);
        new SetupCMD(this);
        new SetupListener(this);
        new BedListener(this);
        new StatsCMD(this);
        new ServerListHandler(this);
        new RettungsListener(this);
        new SpeedTeppichListener(this);
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        scoreboardScore = new net.minecraft.server.v1_8_R3.Scoreboard();
        scoreUtils = new ScoreUtils(this);
        voteManager = new VoteManager(this);

        if (!FileManager.fileExists()){
            FileManager.setConfig();
            FileManager.loadInfo();
        } else {
            FileManager.loadInfo();
        }

        mySQL = new MySQL();

        try {
            MAP = new MapManager().getRandomMap();
            // "Error";
            // new MapManager().getRandomMap();
        }catch (Exception ex){
            MAP = "Error";
        }
        if (MAP.equals("Error")){
            Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§cEs wurde keine Map eingreichtet.");
        } else {
            Bukkit.getConsoleSender().sendMessage(BedWars.PREFIX+"§7Es wurde eine RandomMap ausgesucht: §6"+MAP);
        }

        for (World worlds : Bukkit.getWorlds()){
            worlds.setWeatherDuration(0);
            worlds.setDifficulty(Difficulty.PEACEFUL);
            worlds.setSpawnFlags(false,false);
            worlds.setTime(2000);
            worlds.setGameRuleValue("randomTickSpeed","0");
        }

        later();

        top5Wall = new Top5Wall(this);

    }

    private void later(){
        Bukkit.getScheduler().runTaskLater(this, new Runnable() {
            public void run() {
                replay = Bukkit.getPluginManager().isPluginEnabled("ReplaySystem");
                cloudNet = Bukkit.getPluginManager().isPluginEnabled("CloudNetAPI");
                dynexapi = Bukkit.getPluginManager().isPluginEnabled("Dynex-API");
                labymod = Bukkit.getPluginManager().isPluginEnabled("LabyModAPI");

                if (replay){
                    Bukkit.getConsoleSender().sendMessage(PREFIX+"§9Replay System§7: §aAktiviert");
                } else {
                    Bukkit.getConsoleSender().sendMessage(PREFIX+"§9Replay System§7: §4Deaktiviert");
                }
                if (cloudNet){
                    Bukkit.getConsoleSender().sendMessage(PREFIX+"§9Cloud§7: §aAktiviert");
                } else {
                    Bukkit.getConsoleSender().sendMessage(PREFIX+"§9Cloud§7: §4Deaktiviert");
                }
                if (dynexapi){
                    Bukkit.getConsoleSender().sendMessage(PREFIX+"§9Dynex-API§7: §aAktiviert");
                } else {
                    Bukkit.getConsoleSender().sendMessage(PREFIX+"§9Dynex-API§7: §4Deaktiviert");
                }
                if (labymod){
                    Bukkit.getConsoleSender().sendMessage(PREFIX+"§9LabyMod§7: §aAktiviert");
                } else {
                    Bukkit.getConsoleSender().sendMessage(PREFIX+"§9LabyMod§7: §4Deaktiviert");
                }
                labyModListener = new LabyModListeners(plugin);
            }
        }, 15);
    }


    @Override
    public void onDisable() {
        if (mySQL.isConnected()){
            mySQL.disconnect();
        }
        if (!cloudNet) {
            gameProtection.replace();
        }
    }

    public static BedWars getPlugin() {
        return plugin;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public TeamManager getTeamManager() {
        return teamManager;
    }

    public ItemManager getItemManager() {
        return itemManager;
    }

    public BedUtil getBedUtil() {
        return bedUtil;
    }

    public CountdownManager getCountdownManager() { return countdownManager; }

    public ShopInit getShopInit() {
        return shopInit;
    }

    public ArrayList<Teams> getAllTeams() {
        return allTeams;
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    public ScoreUtils getScoreUtils() {
        return scoreUtils;
    }

    public boolean isForceMap() {
        return forceMap;
    }

    public void setForceMap(boolean forceMap) {
        this.forceMap = forceMap;
    }

    public boolean isCloudNet() {
        return cloudNet;
    }

    public void setCloudNet(boolean cloudNet) {
        this.cloudNet = cloudNet;
    }

    public net.minecraft.server.v1_8_R3.Scoreboard getScoreboardScore() {
        return scoreboardScore;
    }

    public boolean isDynexapi() {
        return dynexapi;
    }

    public boolean isReplay() {
        return replay;
    }

    public boolean isLabymod() {
        return labymod;
    }

    public LabyModListeners getLabyModListener() {
        return labyModListener;
    }

    public boolean isGold() {
        return gold;
    }

    public void setGold(boolean gold) {
        this.gold = gold;
    }

    public VoteManager getVoteManager() {
        return voteManager;
    }

    public boolean isMapVote() {
        return mapVote;
    }

    public void setMapVote(boolean mapVote) {
        this.mapVote = mapVote;
    }

    public boolean isGoldVote() {
        return goldVote;
    }

    public void setGoldVote(boolean goldVote) {
        this.goldVote = goldVote;
    }

    public MySQL getMySQL() {
        return mySQL;
    }

    public Top5Wall getTop5Wall() {
        return top5Wall;
    }

    public GameProtection getGameProtection() {
        return gameProtection;
    }
}