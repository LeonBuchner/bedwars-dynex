package de.nxtlinx.bedwars.gamemanager;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.HashMap;

public class VoteManager {

    private BedWars plugin;

    private HashMap<Player,String> playerVoteMap;
    private ArrayList<String> mapList;
    private HashMap<String,Integer> mapVotes;

    private HashMap<Player,Boolean> goldPlayerVote;
    private HashMap<Boolean,Integer> goldVote;


    public VoteManager(BedWars plugin){
        this.plugin = plugin;
        playerVoteMap = new HashMap<>();
        mapList = new ArrayList<>();
        mapVotes = new HashMap<>();
        goldPlayerVote = new HashMap<>();
        goldVote = new HashMap<>();
        initMap();
    }

    public void addVoteMap(Player player,String map){
      //  removeVoteMap(player);
        if (mapList.contains(map)){
            playerVoteMap.put(player, map);
           if (!mapVotes.containsKey(map)){
               mapVotes.put(map,1);
           } else {
               int votes = mapVotes.get(map);
               votes++;
               mapVotes.put(map,votes);
           }
            player.sendMessage(BedWars.PREFIX+"§7Du hast für die Map §6"+map+"§7 gevotet");
        }
    }

    public void addVoteGold(Player player,boolean gold){
        removeGoldVote(player);
        goldPlayerVote.put(player,gold);
        if (!goldVote.containsKey(gold)){
            goldVote.put(gold,1);
        } else {
            int votes = goldVote.get(gold);
            votes++;
            goldVote.put(gold,votes);
        }
    }

    public void removeAllVotes(Player player){
        removeGoldVote(player);
        removeVoteMap(player);
    }

    public void removeGoldVote(Player player){
        if (goldPlayerVote.containsKey(player)){
            Boolean map = goldPlayerVote.get(player);
            int votes = goldVote.get(map);
            votes--;
            goldVote.put(map,votes);
        }
    }

    public String getWinnnerMap(){
       int max = 0;
       for (int map : mapVotes.values()){
           if (map > max){
               max = map;
           }
       }
       String winner = "NO-Voting";

       for (String mapsHash : mapVotes.keySet()){
           if (mapVotes.get(mapsHash) == max){
               winner = mapsHash;
           }
       }
       return winner;
    }

    public boolean getWinnnerGold(){
        int max = 0;
        for (int map : goldVote.values()){
            if (map > max){
                max = map;
            }
        }
        boolean winner = true;

        for (Boolean bool : goldVote.keySet()){
            if (goldVote.get(bool) == max){
                winner = bool;
            }
        }
        return winner;
    }

    public void removeVoteMap(Player player){
        if (playerVoteMap.containsKey(player)){
            String map = playerVoteMap.get(player);
            int votes = mapVotes.get(map);
            votes--;
            mapVotes.put(map,votes);
        }
    }

    public Inventory getVoteInventoryMap(){
        Inventory inv = Bukkit.createInventory(null, 9 * 3, "§6Map-Voting");
        for (String all : mapList) {
            if (mapVotes.containsKey(all)){
                inv.addItem(new ItemBuilder(Material.PAPER).setDispplayName("§6" + all).addLore("§7Votes §8: §6"+mapVotes.get(all)).build());
            } else {
                inv.addItem(new ItemBuilder(Material.PAPER).setDispplayName("§6" + all).build());
            }
        }
        return inv;
    }

    public Inventory getVoteInventoryGold(){
        Inventory inv = Bukkit.createInventory(null, 9 * 1, "§6Gold-Voting");
        if (goldVote.containsKey(true)){
            inv.setItem(2,new ItemBuilder(Material.INK_SACK,(short)10).setDispplayName("§aAktivieren").addLore("§7Votes §8: §6"+goldVote.get(true)).build());
        } else {
            inv.setItem(2,new ItemBuilder(Material.INK_SACK,(short)10).setDispplayName("§aAktivieren").build());
        }
        if (goldVote.containsKey(false)){
            inv.setItem(6,new ItemBuilder(Material.INK_SACK,(short)1).setDispplayName("§4Deaktivieren").addLore("§7Votes §8: §6"+goldVote.get(false)).build());
        } else {
            inv.setItem(6,new ItemBuilder(Material.INK_SACK,(short)1).setDispplayName("§4Deaktivieren").build());
        }
        return inv;
    }


    private void initMap(){
        MapManager mapManager = new MapManager();
        mapList.addAll(mapManager.getconfiguration().getStringList("Maps"));
    }

    public ArrayList<String> getMapList() {
        return mapList;
    }

    public HashMap<Player, String> getPlayerVoteMap() {
        return playerVoteMap;
    }

    public HashMap<String, Integer> getMapVotes() {
        return mapVotes;
    }

    public HashMap<Player, Boolean> getGoldPlayerVote() {
        return goldPlayerVote;
    }

    public HashMap<Boolean, Integer> getGoldVote() {
        return goldVote;
    }
}
