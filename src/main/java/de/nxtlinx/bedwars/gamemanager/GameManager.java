package de.nxtlinx.bedwars.gamemanager;

import de.dytanic.cloudnet.bridge.CloudServer;
import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.listeners.ServerListHandler;
import de.nxtlinx.bedwars.mysql.Stats;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.teams.TeamIngame;
import de.nxtlinx.bedwars.teams.TeamManager;
import de.nxtlinx.bedwars.teams.Teams;
import de.nxtlinx.bedwars.utils.ScoreUtils;
import de.nxtlinx.bedwars.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import java.util.ArrayList;

public class GameManager {

    private BedWars plugin;
    private GameState gameState;

    private TeamIngame teamIngameManager;
    private TeamManager teamManager;

    private ArrayList<Player> players;
    private ArrayList<Player> spectators;

    private Player playerWinner;
    private Teams teamsWinner;
    private int taskID;
    private double y;

    public static final int MIN_PLAYERS = 2,            // 2 // 3
                            MAX_PLAYERS = 2,            // 2  // 4
                            MAX_PLAYERS_PER_TEAM = 1;   // 1 // 2

    public static String WINNER = "";

    public GameManager(BedWars plugin){
        this.plugin = plugin;
        gameState = GameState.LOBBY;
        players = new ArrayList<>();
        spectators = new ArrayList<>();
        teamIngameManager = new TeamIngame(plugin);
        teamManager = plugin.getTeamManager();
        schedule();
        y = 10;  // 0
    }

    public void schedule(){
        taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            @Override
            public void run() {
                for (World world : Bukkit.getWorlds()){
                  for (Entity entity : world.getEntities()){
                      if (entity instanceof Item){
                          Item item = (Item) entity;
                          if (item.getItemStack().getType() == Material.BED || item.getItemStack().getType() == Material.BED_BLOCK){
                              entity.remove();
                          }
                      }
                  }
                }
            }
        },1*20,1*20);
    }

    public void stopSchedule(){
        if (Bukkit.getScheduler().isCurrentlyRunning(taskID)){
            Bukkit.getScheduler().cancelTask(taskID);
        }
    }

    public void setGameState(GameState gameState){
        this.gameState = gameState;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void startIngame(){
        plugin.getCountdownManager().getLobbyCountdown().cancleIdle();
        plugin.getGameManager().getTeamIngameManager().addAllTeams();
        try {
            teamManagemend();
        } catch (Exception e) {
            sendAll("§cEs gab einen Fehler im Setup, bitte melde diesen schnellstmöglich dem Team");
            sendAll(e.getMessage());
            e.printStackTrace();
        }
        try {
            teleportToSpawn();
        } catch (Exception e) {
            sendAll("§cEs gab einen Fehler im Setup, bitte melde diesen schnellstmöglich dem Team");
            e.printStackTrace();
        }
        ScoreUtils scoreUtils = plugin.getScoreUtils();
        for(Player current : Bukkit.getOnlinePlayers()){
            scoreUtils.removeFromTeam(current);
            scoreUtils.addTeam(current,teamManager.getPlayerTeam(current));
        }
        plugin.getItemManager().startSpawners();
        plugin.getItemManager().spawnShop();

        setGameState(GameState.INGAME);

        for(Player current : Bukkit.getOnlinePlayers()){
            current.setLevel(1);
            ScoreUtils.setPacketScore(current);
            if (plugin.getMySQL().isConnected()){
                try {
                    Stats.addPlayed(current.getUniqueId().toString(),1);
                }catch (Exception ex){ }
            }
         //   current.setSaturation(20);
        }
        new ServerListHandler(plugin);

        if (plugin.isCloudNet()){
            CloudServer.getInstance().changeToIngame();
        }
        try {
            y = new MapManager().getDeathHigh(BedWars.MAP);
        }catch (Exception ex){
            y = 0d;
        }
    }

    private void teamManagemend() throws Exception{
        TeamManager teamManager = plugin.getTeamManager();

        for(Player current : players){
            if(teamManager.getPlayerTeam(current) == null){
                teamManager.sortInPLayer(current);
            }
        }

        teamManager.balanceTeams();
        for(Player current : players){
            current.sendMessage(BedWars.PREFIX + "§7Du bist nun in Team " + teamManager.getPlayerTeam(current).getPREFIX() + "§7");
        }

        teamIngameManager.addAllTeams();

    }

    private void teleportToSpawn() throws Exception{
        for (Player all : Bukkit.getOnlinePlayers()){
            Teams teams = teamManager.getPlayerTeam(all);
            all.getInventory().clear();
            if (teams != null){
                all.teleport(new MapManager().getMapLocation(plugin.MAP,teams));
                all.sendMessage(BedWars.PREFIX+"§aEs wurden alle auf die Map teleportiert");
            } else {
                all.sendMessage(BedWars.PREFIX+"§cEs gab einen Fehler im Setup, bitte melde diesen schnellstmöglich dem Team");
            }
        }
    }

    private void sendAll(String message){
        for (Player player : Bukkit.getOnlinePlayers()){
            player.sendMessage(BedWars.PREFIX+message);
        }
    }

    public void startEnding(){
        gameState = GameState.ENDING;
        plugin.getItemManager().cancelSpawning();

            for (Player all: Bukkit.getOnlinePlayers()){
                for (Player spieler : players){
                    TeamManager teamManager = plugin.getTeamManager();
                    Teams teams = teamManager.getPlayerTeam(spieler);
                    if (teams != null){
                        all.sendTitle(teams.getPREFIX()+"§a hat","§agewonnen");
                        teamsWinner = teams;
                    }
                }
            }


        plugin.getCountdownManager().getEndCountdown().run();

        for (Player playerAll: Bukkit.getOnlinePlayers()){
            playerAll.getInventory().clear();
            playerAll.getInventory().setArmorContents(null);
            playerAll.updateInventory();

            try {
                playerAll.teleport(new MapManager().getLobby());
            } catch (Exception e) {
                playerAll.sendMessage(BedWars.PREFIX + "§cEs gab einen Fehler im Setup, bitte melde diesen schnellstmöglich dem Team");
            }
            for (Player spectator : spectators){
                playerAll.showPlayer(spectator);
            }
        }

        for (Player allPlayers : Bukkit.getOnlinePlayers()){
            Utils.setEndingItems(allPlayers);
        }

        if (plugin.getMySQL().isConnected()){
            try {
                Stats.addWinns(playerWinner.getUniqueId().toString(),1);
            }catch (Exception ex){ }
        }




        new ServerListHandler(plugin);
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public ArrayList<Player> getSpectators() {
        return spectators;
    }

    public TeamIngame getTeamIngameManager() {
        return teamIngameManager;
    }

    public Player getPlayerWinner() {
        return playerWinner;
    }

    public Teams getTeamsWinner() {
        return teamsWinner;
    }

    public double getY() {
        return y;
    }
}
