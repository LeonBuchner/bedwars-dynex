package de.nxtlinx.bedwars.commands;

import de.nxtlinx.bedwars.BedWars;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class BuildCMD implements CommandExecutor {

    private BedWars plugin;

    public static ArrayList<Player> build = new ArrayList<Player>();

    public BuildCMD(BedWars plugin){
        this.plugin = plugin;
        plugin.getCommand("build").setExecutor(this);

    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player){
            Player player = (Player) sender;
            if (player.hasPermission("system.build")){
                if (!build.contains(player)){
                    build.add(player);
                    player.getInventory().clear();
                    player.setGameMode(GameMode.CREATIVE);
                    player.sendMessage(BedWars.PREFIX+"§7Du hast den Build Modus §aaktiviert");
                } else {
                    build.remove(player);
                    player.getInventory().clear();
                    player.setGameMode(GameMode.ADVENTURE);
                    player.sendMessage(BedWars.PREFIX+"§7Du hast den Build Modus §4deaktiviert");
                }
            }
        }
        return false;
    }
}
