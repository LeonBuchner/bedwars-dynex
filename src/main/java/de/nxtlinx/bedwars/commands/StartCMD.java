package de.nxtlinx.bedwars.commands;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.countdown.LobbyCountdown;
import de.nxtlinx.bedwars.gamemanager.GameManager;
import de.nxtlinx.bedwars.gamemanager.GameState;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartCMD implements CommandExecutor {

    private BedWars plugin;

    public StartCMD(BedWars plugin){
        this.plugin = plugin;
        this.plugin.getCommand("start").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player){
            Player player = (Player) sender;
            if (player.hasPermission("bw.start")){
                if (plugin.getGameManager().getGameState() == GameState.LOBBY) {

                    LobbyCountdown ls = plugin.getCountdownManager().getLobbyCountdown();

                    if (ls.isRunning()) {

                        if (!ls.isStarting()) {

                            ls.setSeconds(10);
                            player.sendMessage(BedWars.PREFIX + "§7Du hast erfolgreich die Runde gestartet!");
                            ls.setStarting(true);

                        } else
                            player.sendMessage(BedWars.PREFIX + "§cDas Spiel startet bereits!");

                    } else{
                        int missing = GameManager.MIN_PLAYERS - plugin.getGameManager().getPlayers().size();
                        player.sendMessage(BedWars.PREFIX + "§cEs fehlen noch §b"+missing+ " §cSpieler bis zum Start des Countdowns!");
                    }

                } else
                    player.sendMessage(BedWars.PREFIX + "§cDie Lobby-Phase ist bereits beendet!");
            }
        }
        return false;
    }
}
