package de.nxtlinx.bedwars.commands;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.gamemanager.GameState;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class ForceMapCMD implements CommandExecutor {

    private BedWars plugin;

    public ForceMapCMD(BedWars plugin){
        this.plugin = plugin;
        this.plugin.getCommand("forcemap").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("bw.forcemap")) {
                if (plugin.isForceMap()) {
                    if (plugin.getGameManager().getGameState() == GameState.LOBBY){
                        Inventory inv = Bukkit.createInventory(null, 9 * 3, "§6ForceMap");
                        MapManager mapManager = new MapManager();
                        for (String all : mapManager.getconfiguration().getStringList("Maps")) {
                            inv.addItem(new ItemBuilder(Material.PAPER).setDispplayName("§6" + all).build());
                        }
                        player.openInventory(inv);
                    } else {
                        player.sendMessage(BedWars.PREFIX + "§cDu kannst im Spiel nicht die Map ändern.");
                    }
                } else {
                    player.sendMessage(BedWars.PREFIX + "§cAktuell kannst du die Map nicht mehr ändern.");
                }
            }
        }
        return false;
    }
}
