package de.nxtlinx.bedwars.commands;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.setup.ItemManager;
import de.nxtlinx.bedwars.setup.MapManager;
import de.nxtlinx.bedwars.setup.SetupInit;
import de.nxtlinx.bedwars.teams.Item;
import de.nxtlinx.bedwars.teams.Teams;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class SetupCMD implements CommandExecutor {

    private BedWars plugin;
    private ItemManager itemManager;

    public static HashMap<Player, SetupInit> setup = new HashMap<>();

    public SetupCMD(BedWars plugin){
        this.plugin = plugin;
        this.plugin.getCommand("setup").setExecutor(this);
        this.itemManager = plugin.getItemManager();
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] strings) {
        if (sender instanceof Player){
            Player player = (Player) sender;
            if (player.hasPermission("bw.setup")){
                if (strings.length == 0){
                    help(player);
                }
                if (strings.length == 1){
                    if (strings[0].equalsIgnoreCase("lobby")){
                        new MapManager().setLobby(player.getLocation());
                        player.sendMessage(BedWars.PREFIX+"§7Du hast den Lobby Spawn gesetzt");

                    } else if (strings[0].equalsIgnoreCase("listcolors")){
                        sendTeamColors(player);

                    } else {
                        help(player);
                    }
                }
                if (strings.length == 2){
                    String mapName = strings[0].toUpperCase();
                    player.sendMessage(BedWars.PREFIX+"§7Map wurde erkannt: §6"+mapName);
                    if (strings[1].equalsIgnoreCase("shop")){
                         int i = itemManager.setShop(player.getLocation(),mapName);
                        player.sendMessage(BedWars.PREFIX+"§7Du hast den Shop Spawn gesetzt. §7ID§8: §6"+i);
                    }
                    if (strings[1].equalsIgnoreCase("death")){
                        MapManager mapManager = new MapManager();
                        if (mapManager.getconfiguration().getStringList("Maps").contains(mapName)){
                            mapManager.setDeathHigh(player.getLocation(),mapName);
                            player.sendMessage(BedWars.PREFIX+"§7Du hast die Höhe eingestellt, auf welcher die Spieler sterben!");
                        } else {
                            player.sendMessage(BedWars.PREFIX+"§7Diese Map §6"+mapName+"§7 existiert nicht");
                        }
                    }
                }

                if (strings.length == 3){
                    String mapName = strings[0].toUpperCase();
                    player.sendMessage(BedWars.PREFIX+"§7Map wurde erkannt: §6"+mapName);
                    if (strings[1].equalsIgnoreCase("spawn")){
                        if (strings[2].equalsIgnoreCase("spec")){
                            MapManager mapManager = new MapManager();
                            if (mapManager.mapExists(mapName)){
                                mapManager.createSpec(player.getLocation(),mapName);
                                send(player,"§7Du hast den Spectator Spawn gesetzt");
                            } else {
                                player.sendMessage(BedWars.PREFIX+"§7Diese Map §6"+mapName+"§7 existiert nicht");
                            }
                        } else {
                            String colorTeam = strings[2].toUpperCase();
                            try {
                                Teams teamChoos = Teams.valueOf(colorTeam);
                                if (plugin.getAllTeams().contains(teamChoos)){
                                    new MapManager().createMap(mapName, player.getLocation(), teamChoos);
                                    player.sendMessage(BedWars.PREFIX + "§7Du hast den Spawn von " + teamChoos.getChatColor() + teamChoos.getTeamName() + " §7gesetzt");
                                } else {
                                    player.sendMessage(BedWars.PREFIX+"§7Dieses Team ist aktuell nicht verfügbar");
                                }
                            } catch (IllegalArgumentException ex) {
                                send(player, "§cDiese Farbe ist Falsch!");
                                sendTeamColors(player);
                            }
                        }
                    }

                    if (strings[1].equalsIgnoreCase("bed")){
                            String colorTeam = strings[2].toUpperCase();
                            try {
                                Teams teamChoos = Teams.valueOf(colorTeam);
                                if (plugin.getAllTeams().contains(teamChoos)){
                                    setup.put(player,new SetupInit(teamChoos,mapName));
                                    player.sendMessage(BedWars.PREFIX + "§7Interagiere nun mit dem Bett von " + teamChoos.getChatColor() + teamChoos.getTeamName());
                                } else {
                                    player.sendMessage(BedWars.PREFIX+"§7Dieses Team ist aktuell nicht verfügbar");
                                }
                            } catch (IllegalArgumentException ex) {
                                send(player, "§cDiese Farbe ist Falsch!");
                                sendTeamColors(player);
                            }

                    }

                    if (strings[1].equalsIgnoreCase("spawner")){
                        MapManager mapManager = new MapManager();
                        if (mapManager.mapExists(mapName)) {
                            String itemType = strings[2].toUpperCase();
                            try {
                                Item itemChoos = Item.valueOf(itemType);
                                int i = itemManager.setSpawn(itemChoos, player.getLocation(), mapName);
                                player.sendMessage(BedWars.PREFIX + "§7Du hast den Spawner §6" + itemChoos.name() + " §7gesetzt. §7ID§8: §6" + i);
                            } catch (IllegalArgumentException ex) {
                                send(player, "§cDieses Item ist Falsch!");
                            }
                        } else {
                            player.sendMessage(BedWars.PREFIX+"§7Diese Map §6"+mapName+"§7 existiert nicht");
                        }
                    }

                    if (strings[1].equalsIgnoreCase("builder")){
                        MapManager mapManager = new MapManager();
                        if (mapManager.mapExists(mapName)) {
                            String builder = strings[2];
                            new MapManager().setBuilder(mapName,builder);
                            player.sendMessage(BedWars.PREFIX+"§7Du hast den Erbauer der Map §6"+mapName+" §7auf §6"+builder+" §7gesetzt");
                        } else {
                            player.sendMessage(BedWars.PREFIX+"§7Diese Map §6"+mapName+"§7 existiert nicht");
                        }
                    }
                }
            }
        }
        return false;
    }

    private void help(Player player){
        send(player,"§7/setup lobby");
        send(player,"§7/setup listcolors");
        send(player,"§7/setup <mapname> spawn spec");
        send(player,"§7/setup <mapname> spawn <COLOR>");
        send(player,"§7/setup <mapname> bed <COLOR>");
        send(player,"§7/setup <mapname> spawner <GOLD,EISEN,BRONZE>");
        send(player,"§7/setup <mapname> shop");
        send(player,"§7/setup <mapname> death");
        send(player,"§7/setup <mapname> builder <name>");
    }

    private void sendTeamColors(Player player){
        send(player,"§7Folgende Farben sind aktiv§8:");
        for (Teams teams : plugin.getAllTeams()){
            send(player,teams.getChatColor()+teams.name());
        }
    }

    private void send(Player player,String message){
        player.sendMessage(BedWars.PREFIX+message);
    }
}
