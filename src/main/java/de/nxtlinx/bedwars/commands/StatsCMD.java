package de.nxtlinx.bedwars.commands;

import de.nxtlinx.bedwars.BedWars;
import de.nxtlinx.bedwars.mysql.Stats;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class StatsCMD implements CommandExecutor {

    private BedWars plugin;

    public StatsCMD(BedWars plugin){
        this.plugin = plugin;
        plugin.getCommand("stats").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] strings) {
        if (sender instanceof Player){
            Player player = (Player) sender;
            if (plugin.getMySQL().isConnected()){
                if (strings.length == 0){
                    if (Stats.playerExists(player.getUniqueId().toString())){
                        int kills = Stats.getKills(player.getUniqueId().toString());
                        int death = Stats.getDeaths(player.getUniqueId().toString());
                        double kd = (double) kills / (double) death;
                        DecimalFormat fom = new DecimalFormat("#.##");
                        String test = fom.format(kd);
                        String KILLSDEATH = String.valueOf(test);
                        Integer platz = plugin.getTop5Wall().getAllPlatz().get(player.getName());
                        player.sendMessage("§7§m----------§m§3Bed§bWars§7§m----------");
                        player.sendMessage(BedWars.PREFIX + "§7Platz: §e"+platz);
                        player.sendMessage(BedWars.PREFIX + "§7Kills: §e" + kills);
                        player.sendMessage(BedWars.PREFIX + "§7Deaths: §e" + death);
                        player.sendMessage(BedWars.PREFIX + "§7KD: §e" + KILLSDEATH);
                        player.sendMessage("§7§m----------§m§3Bed§bWars§7§m----------");
                    } else {
                        player.sendMessage(BedWars.PREFIX+"§cDu bist nicht in der Stats DB!");
                    }

                }
                if (strings.length == 1){
                    String nameCMD = strings[0];
                    if (Stats.playerExistsByName(nameCMD)){
                        String uuid = Stats.getUUID(nameCMD);
                        int kills = Stats.getKills(uuid);
                        int death = Stats.getDeaths(uuid);
                        double kd = (double) kills / (double) death;
                        DecimalFormat fom = new DecimalFormat("#.##");
                        String test = fom.format(kd);
                        String KILLSDEATH = String.valueOf(test);
                        Integer platz = plugin.getTop5Wall().getAllPlatz().get(nameCMD);
                        player.sendMessage("§7§m----------§m§3Bed§bWars§7§m----------");
                        player.sendMessage("§7Stats von §e"+nameCMD);
                        player.sendMessage(BedWars.PREFIX + "§7Platz: §e"+platz);
                        player.sendMessage(BedWars.PREFIX + "§7Kills: §e" + kills);
                        player.sendMessage(BedWars.PREFIX + "§7Deaths: §e" + death);
                        player.sendMessage(BedWars.PREFIX + "§7KD: §e" + KILLSDEATH);
                        player.sendMessage("§7§m----------§m§3Bed§bWars§7§m----------");
                    } else {
                        player.sendMessage(BedWars.PREFIX+"§cDieser Spieler ist nicht in der Stats DB!");
                    }
                }
            } else {
                player.sendMessage(BedWars.PREFIX+"§cAktuell ist das Stats System deaktiviert");
            }
        }
        return false;
    }
}
